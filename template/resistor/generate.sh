#!/bin/bash
for asd in 0 1 2 3 4 5 6 7 8 9 \
10 15 22 33 47 68 \
100 150 220 330 470 680 \
1000 1500 2200 3300 4700 6800 \
10000 15000 22000 33000 47000 68000 \
100000 150000 220000 330000 470000 680000 \
1000000 1500000 2200000 3300000 4700000 6800000 \
10000000 15000000 22000000 33000000 47000000 68000000 \
100000000 150000000 220000000 330000000 470000000 680000000 \
1000000000 1500000000 2200000000 3300000000 4700000000 6800000000 \
10000000000 15000000000 22000000000 33000000000 47000000000 68000000000 \
100000000000 150000000000 220000000000 330000000000 470000000000 680000000000 \
; do
RESISTANCE="${asd}"

# Do not modify the code below
# basic color index
BASIC_INDEX[0]="#000000" #black
BASIC_INDEX[1]="#880C0C" #brown
BASIC_INDEX[2]="#FF0000" #red
BASIC_INDEX[3]="#FFA800" #orange
BASIC_INDEX[4]="#FFFF00" #yellow
BASIC_INDEX[5]="#008A00" #green
BASIC_INDEX[6]="#00008A" #blue
BASIC_INDEX[7]="#FF00FF" #purgle
BASIC_INDEX[8]="#888888" #grey
BASIC_INDEX[9]="#FFFFFF" #white
# tolerance color index
TOLERANCE_INDEX[0]="#C0C800" #gold
TOLERANCE_INDEX[1]="#C8C8C8" #silver

INDEX_3=`echo "l(${RESISTANCE}) / l(E) - 1" | bc -l`
INDEX_3=`printf "%.0f" "${INDEX_3}"`

REMAIND=`echo "${RESISTANCE} / (10 ^ (${INDEX_3}))" | bc`
INDEX_2=`echo "${REMAIND} % 10" | bc`

REMAIND=`echo "${REMAIND} / 10" | bc`
INDEX_1=`echo "${REMAIND} % 10" | bc`

BAND_1_COLOR="${BASIC_INDEX[${INDEX_1}]}"
BAND_2_COLOR="${BASIC_INDEX[${INDEX_2}]}"
BAND_3_COLOR="${BASIC_INDEX[${INDEX_3}]}"

if [ "${RESISTANCE}" -ge 1000000000 ]; then
	RESISTANCE=`echo "${RESISTANCE} / 1000000000" | bc -l | awk '{print $1 + 0}'`"G"
elif [ "${RESISTANCE}" -ge 1000000 ]; then
	RESISTANCE=`echo "${RESISTANCE} / 1000000" | bc -l | awk '{print $1 + 0}'`"M"
elif [ "${RESISTANCE}" -ge 1000 ]; then
	RESISTANCE=`echo "${RESISTANCE} / 1000" | bc -l | awk '{print $1 + 0}'`"K"
fi
BAND_1_COLOR=`echo "${BAND_1_COLOR}" | tr "[[:lower:]]" "[[:upper:]]"`
BAND_2_COLOR=`echo "${BAND_2_COLOR}" | tr "[[:lower:]]" "[[:upper:]]"`
BAND_3_COLOR=`echo "${BAND_3_COLOR}" | tr "[[:lower:]]" "[[:upper:]]"`

PREFIX="resistor"

ICON="icon"
BREADBOARD="breadboard"
SCHEMATIC="schematic"
PCB="pcb"
PART="part"
DIST="dist"

mkdir "${ICON}"
mkdir "${BREADBOARD}"
mkdir "${SCHEMATIC}"
mkdir "${PCB}"
mkdir "${PART}"
mkdir "${DIST}"

UUID=`uuidgen`
part=`cat "${PART}.${PREFIX}-template.fzp"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{UUID\\\\}/${UUID}/g"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{RESISTANCE\\\\}/${RESISTANCE}/g"`

breadboard=`cat "svg.${BREADBOARD}.${PREFIX}-template.svg"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{BAND_1_COLOR\\\\}/${BAND_1_COLOR}/g"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{BAND_2_COLOR\\\\}/${BAND_2_COLOR}/g"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{BAND_3_COLOR\\\\}/${BAND_3_COLOR}/g"`

icon=`cat "svg.${ICON}.${PREFIX}-template.svg"`
icon=`echo "${icon}" | sed -r "s/\\\\$\\\\{BAND_1_COLOR\\\\}/${BAND_1_COLOR}/g"`
icon=`echo "${icon}" | sed -r "s/\\\\$\\\\{BAND_2_COLOR\\\\}/${BAND_2_COLOR}/g"`
icon=`echo "${icon}" | sed -r "s/\\\\$\\\\{BAND_3_COLOR\\\\}/${BAND_3_COLOR}/g"`

schematic=`cat "svg.${SCHEMATIC}.${PREFIX}-template.svg"`
schematic=`echo "${schematic}" | sed -r "s/\\\\$\\\\{RESISTANCE\\\\}/${RESISTANCE}/g"`

pcb=`cat "svg.${PCB}.${PREFIX}-template.svg"`
pcb=`echo "${pcb}" | sed -r "s/\\\\$\\\\{RESISTANCE\\\\}/${RESISTANCE}/g"`

FILENAME=`echo "${PREFIX}-${RESISTANCE}" | tr "[[:upper:]]" "[[:lower:]]"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{FILENAME\\\\}/${FILENAME}/g"`

echo -n "${icon}" >"${ICON}/${FILENAME}.svg"
echo -n "${breadboard/\$\{CHAIN\}/${breadboard_g}}" >"${BREADBOARD}/${FILENAME}.svg"
echo -n "${schematic}" >"${SCHEMATIC}/${FILENAME}.svg"
echo -n "${pcb}" >"${PCB}/${FILENAME}.svg"
echo -n "${part/\$\{CHAIN\}/${part_g}}" >"${PART}/${FILENAME}.fzp"

ln -s "${ICON}/${FILENAME}.svg" "svg.${ICON}.${FILENAME}.svg"
ln -s "${BREADBOARD}/${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg"
ln -s "${SCHEMATIC}/${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg"
ln -s "${PCB}/${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg"
ln -s "${PART}/${FILENAME}.fzp" "${PART}.${FILENAME}.fzp"

zip \
"${DIST}/${FILENAME}.fzpz" \
"svg.${ICON}.${FILENAME}.svg" \
"svg.${BREADBOARD}.${FILENAME}.svg" \
"svg.${SCHEMATIC}.${FILENAME}.svg" \
"svg.${PCB}.${FILENAME}.svg" \
"${PART}.${FILENAME}.fzp" \

rm "svg.${ICON}.${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg" "${PART}.${FILENAME}.fzp"
done