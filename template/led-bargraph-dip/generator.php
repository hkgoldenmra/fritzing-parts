<?php

// global settings
$globalColor = "Red";
$globalSegments = 1;
$globalTitle = sprintf("LED Bargraph DIP %s %s", $globalColor, $globalSegments);
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "LEDBAR";
$fritzingTags = array("LED", "Bargraphic", "DIP");
$fritzingProperties = array(
	"color" => $globalColor,
	"family" => "A LED Bargraph",
	"package" => "DIP",
	"segments" => $globalSegments,
);

// icon settings
$iconUnit = "in";
$iconShieldColor = "#000000";
$iconLEDWidth = 6;
$iconLEDHeight = 24;
$iconPokoColor = "#444444";

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardShieldColor = $iconShieldColor;
$breadboardLEDWidth = $iconLEDWidth;
$breadboardLEDHeight = $iconLEDHeight;
$breadboardPokoColor = $iconPokoColor;
$breadboardPinRadius = 2.5;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;

// Don't change the code below, unless you know what to do...
function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

// debug
$debugMinSegments = 1;
if ($globalSegments < $debugMinSegments) {
	$globalSegments = $debugMinSegments;
}

$globalInterval = 10;
$iconWidth = 40;
$iconHeight = 40;
$breadboardWidth = $globalSegments * $globalInterval;
$breadboardHeight = $iconHeight;
$pcbWidth = $globalSegments * $globalInterval;
$pcbHeight = $iconHeight;
$schematicWidth = $globalSegments * $globalInterval;
$schematicHeight = 30;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("led-bargraph-dip-%s-%s", $globalColor, $globalSegments);
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
function toPartConnectorArray($segment, $side) {
	$name = sprintf("%s%s", $segment, (($side == "+") ? "+" : "-"));
	$id = sprintf("connector-%s", $name);
	return array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => $name,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => sprintf("Segment %s %s", $segment, (($side == "+") ? "Positive" : "Negative")),
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	);
}

$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
for ($partI = 0; $partI < $globalSegments; $partI++) {
	array_unshift($fritzingConnectorsChain, toPartConnectorArray($globalSegments - $partI, "-"));
	array_push($fritzingConnectorsChain, toPartConnectorArray($globalSegments - $partI, "+"));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$iconColumnLEDsChain = array();
for ($iconI = 0; $iconI < $iconWidth / 10; $iconI++) {
	$iconX = $iconI * $globalInterval;
	array_push($iconColumnLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0)", $iconX),
		),
	));
}
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "led-template",
						"x" => $iconLEDWidth / -2,
						"y" => $iconLEDHeight / -2,
						"width" => $iconLEDWidth,
						"height" => $iconLEDHeight,
						"fill" => $globalColor,
						"stroke" => "none",
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "circle",
							"attributes" => array(
								"cx" => $iconWidth / 2,
								"cy" => $iconHeight,
								"r" => 5,
								"fill" => $breadboardPokoColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(5,0)",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $iconHeight / 2),
									),
									"tags" => $iconColumnLEDsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardLEDsChain = array();
$breadboardFrontPinsChain = array();
$breadboardBackPinsChain = array();
for ($breadboardI = 0; $breadboardI < $globalSegments; $breadboardI++) {
	$breadboardX = $breadboardI * $globalInterval;
	array_push($breadboardLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0)", $breadboardX),
		),
	));
	array_push($breadboardFrontPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s--pin", $breadboardI + 1),
			"xlink:href" => (($breadboardI > 0) ? "#pin-template" : "#1st-pin-template"),
			"transform" => sprintf("translate(%s,0)", $breadboardX),
		),
	));
	array_push($breadboardBackPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s+-pin", $breadboardI + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $breadboardX),
		),
	));
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "led-template",
						"x" => $breadboardLEDWidth / -2,
						"y" => $breadboardLEDHeight / -2,
						"width" => $breadboardLEDWidth,
						"height" => $breadboardLEDHeight,
						"fill" => $globalColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "1st-pin-template",
						"x" => -$breadboardPinRadius,
						"y" => -$breadboardPinRadius,
						"width" => $breadboardPinRadius * 2,
						"height" => $breadboardPinRadius * 2,
						"opacity" => 0,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $breadboardPinRadius,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardShieldColor,
							),
						),
						array(
							"name" => "circle",
							"attributes" => array(
								"cx" => $breadboardWidth / 2,
								"cy" => $breadboardHeight,
								"r" => 5,
								"fill" => $breadboardPokoColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $globalInterval / 2, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,0)",
									),
									"tags" => $breadboardLEDsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / 2),
									),
									"tags" => $breadboardFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / -2),
									),
									"tags" => array_reverse($breadboardBackPinsChain),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontPinsChain = array();
$pcbBackPinsChain = array();
for ($pcbI = 0; $pcbI < $globalSegments; $pcbI++) {
	$pcbX = $pcbI * $globalInterval;
	array_push($pcbFrontPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s--pin", $pcbI + 1),
			"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
			"transform" => sprintf("translate(%s,0)", $pcbX),
		),
	));
	array_push($pcbBackPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s+-pin", $pcbI + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $pcbX),
		),
	));
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"opacity" => 0,
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"id" => "direction-template",
							),
							"tags" => array(
								array(
									"name" => "polygon",
									"attributes" => array(
										"points" => "-1,0 1,0 0,2",
										"fill" => $pcbSilkscreenColor,
										"stroke" => "none",
									),
								),
								array(
									"name" => "polyline",
									"attributes" => array(
										"points" => "0,-2 0,0",
										"fill" => "none",
										"stroke" => $pcbSilkscreenColor,
										"stroke-width" => $pcbSilkscreenThickness / 2,
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"id" => "led-template",
							),
							"tags" => array(
								array(
									"name" => "polygon",
									"attributes" => array(
										"points" => "-4,-5 4,-5 0,5",
										"fill" => "none",
										"stroke" => $pcbSilkscreenColor,
										"stroke-width" => $pcbSilkscreenThickness,
									),
								),
								array(
									"name" => "polyline",
									"attributes" => array(
										"points" => "0,-10 0,10",
										"fill" => "none",
										"stroke" => $pcbSilkscreenColor,
										"stroke-width" => $pcbSilkscreenThickness,
									),
								),
								array(
									"name" => "polyline",
									"attributes" => array(
										"points" => "-4,5 4,5",
										"fill" => "none",
										"stroke" => $pcbSilkscreenColor,
										"stroke-width" => $pcbSilkscreenThickness,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(2.5,7.5)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"transform" => "rotate(-45)",
												"xlink:href" => "#direction-template",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(2.5,9.5)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"transform" => "rotate(-45)",
												"xlink:href" => "#direction-template",
											),
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 0,
						"y" => 0,
						"width" => $pcbWidth,
						"height" => $pcbHeight,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "use",
					"attributes" => array(
						"transform" => sprintf("translate(%s,20)", $breadboardWidth / 2),
						"xlink:href" => "#led-template",
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => "none",
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => "none",
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => "none",
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $globalInterval / 2, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / 2),
									),
									"tags" => $pcbFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / -2),
									),
									"tags" => array_reverse($pcbBackPinsChain),
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicLEDsChain = array();
$schematicFrontPinsChain = array();
$schematicFrontTerminalsChain = array();
$schematicBackPinsChain = array();
$schematicBackTerminalsChain = array();
$schematicI = 0;
for ($schematicI = 0; $schematicI < $globalSegments; $schematicI++) {
	$schematicX = $schematicI * $globalInterval;
	array_push($schematicLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0)", $schematicX),
		),
	));
	array_push($schematicFrontPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s--pin", $schematicI + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $schematicX),
		),
	));
	array_push($schematicFrontTerminalsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s--terminal", $schematicI + 1),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(%s,0)", $schematicX),
		),
	));
	array_push($schematicBackPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s+-pin", $schematicI + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $schematicX),
		),
	));
	array_push($schematicBackTerminalsChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s+-terminal", $schematicI + 1),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(%s,0)", $schematicX),
		),
	));
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "direction-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-1,0 1,0 0,2",
								"fill" => $schematicBorderColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-2 0,0",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness / 2,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-4,-5 4,-5 0,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-5 0,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "-4,5 4,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(3,8)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,0) rotate(-45)",
										"xlink:href" => "#direction-template",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,2) rotate(-45)",
										"xlink:href" => "#direction-template",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "0,-5 0,5",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(5,0)",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,15)",
							),
							"tags" => $schematicLEDsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,25)",
							),
							"tags" => $schematicFrontPinsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,30)",
							),
							"tags" => $schematicFrontTerminalsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,5)",
							),
							"tags" => array_reverse($schematicBackPinsChain),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,0)",
							),
							"tags" => array_reverse($schematicBackTerminalsChain),
						),
					),
				),
//				array(
//					"name" => "g",
//					"attributes" => array(
//						"transform" => sprintf("translate(%s,10)", $schematicWidth / 2),
//					),
//					"tags" => array(
//						array(
//							"name" => "text",
//							"attributes" => array(
//								"fill" => $schematicTitleColor,
//								"stroke" => "none",
//								"font-family" => $globalFontFamily,
//								"font-size" => $schematicTitleSize,
//								"text-anchor" => "middle",
//								"transform" => "rotate(90)",
//								"x" => count($globalPins) * 2.5 - 5,
//								"y" => fontBaselineOffset($schematicTitleSize),
//							),
//							"tags" => $globalTitle,
//						),
//						array(
//							"name" => "g",
//							"attributes" => array(
//								"transform" => sprintf("translate(%s,0)", $schematicWidth / -2 + 10),
//							),
//							"tags" => array(
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => "translate(0,0)",
//									),
//									"tags" => $schematicFrontPinsChain,
//								),
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => "translate(-10,0)",
//									),
//									"tags" => $schematicFrontTerminalsChain,
//								),
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => sprintf("translate(5,%s)", -fontBaselineOffset($schematicTextSize) * 2),
//										"fill" => $schematicTextColor,
//										"font-family" => $globalFontFamily,
//										"font-size" => $schematicTextSize,
//										"stroke" => "none",
//										"text-anchor" => "end",
//									),
//									"tags" => $schematicLeftTextsChain,
//								),
//							),
//						),
//						array(
//							"name" => "g",
//							"attributes" => array(
//								"transform" => sprintf("translate(%s,0)", $schematicWidth / 2 - 10),
//							),
//							"tags" => array(
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => "translate(0,0)",
//									),
//									"tags" => $schematicBackPinsChain,
//								),
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => "translate(10,0)",
//									),
//									"tags" => $schematicBackTerminalsChain,
//								),
//								array(
//									"name" => "g",
//									"attributes" => array(
//										"transform" => sprintf("translate(-5,%s)", -fontBaselineOffset($schematicTextSize) * 2),
//										"fill" => $schematicTextColor,
//										"font-family" => $globalFontFamily,
//										"font-size" => $schematicTextSize,
//										"stroke" => "none",
//										"text-anchor" => "start",
//									),
//									"tags" => $schematicRightTextsChain,
//								),
//							),
//						),
//					),
//				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}