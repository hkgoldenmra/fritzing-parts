<?php

$globalPins = array(
	"VCC" => "Positive Power Supply (Red)",
	"D-" => "Data- (2.0) (White)",
	"D+" => "Data+ (2.0) (Green)",
	"P.GND" => "Power Ground (Black)",
	"RX-" => "Superspeed Receiver- (Blue)",
	"RX+" => "Superspeed Receiver+ (Yellow)",
	"S.GND" => "Signal Ground (Grey)",
	"TX-" => "Superspeed Transmitter- (Purple)",
	"TX+" => "Superspeed Transmitter+ (Orange)",
	"SBL.GND" => "Shield Back Left Ground",
	"SBR.GND" => "Shield Back Right Ground",
	"SFL.GND" => "Shield Front Left Ground",
	"SFR.GND" => "Shield Front Right Ground",
);
$globalTitle = "USB 3.0 Type-A";
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");
define("SVG_XML_UUID", "f6f8bddf-c460-4f93-847d-0f2f6dd10f99");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "USB";
$fritzingProperties = array(
	"family" => sprintf("A %s", $fritzingLabel),
	"package" => "THT",
	"pins" => count($globalPins),
	"size" => "Standard",
	"type" => "A",
	"version" => "3.0",
);
$fritzingTags = array($fritzingLabel, $fritzingProperties["package"]);

// icon settings
$iconUnit = "mm";
$iconPinWidth = 1.2;
$iconPinHeight = 0.5;
$iconShieldColor = "#DDDDDD";
$iconVersionColor = "#33AAEE";

// breadboard settings
$breadboardUnit = "in";
$breadboardMagnify = 2.5;
$breadboardShieldColor = $iconShieldColor;
$breadboardVersionColor = $iconVersionColor;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinThickness = 2;
$pcbPinRadius = 5;
$pcbShieldRadius = 12.5;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTextSize = 10;
$pcbTitleSize = 10;

// schematic settings
$schematicUnit = $breadboardUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 3;

// Don't change the code below, unless you know what to do...
function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 12;
$iconHeight = 3.945;
$breadboardWidth = 70;
$breadboardHeight = 40;
$pcbMultiple = 10;
$pcbWidth = 175;
$pcbHeight = 115;
$schematicWidth = 100;
$schematicHeight = 70;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("usb-3.0-type-a");
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
foreach ($globalPins as $key => $value) {
	$id = sprintf("connector-%s", $key);
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => $key,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => $value,
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => ((defined("SVG_XML_UUID")) ? SVG_XML_UUID : uuidgen()),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "pin-3.0-template",
						"x" => $iconPinHeight / -2,
						"y" => $iconPinHeight / -2,
						"width" => $iconPinHeight,
						"height" => $iconPinHeight,
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "pin-template",
						"x" => $iconPinWidth / -2,
						"y" => $iconPinHeight / -2,
						"width" => $iconPinWidth,
						"height" => $iconPinHeight,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "#000000",
						"stroke-width" => 0.1,
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"rx" => 0.5,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0.75,
								"y" => 0.75,
								"width" => 10.5,
								"height" => 1.5,
								"rx" => 0.2,
								"fill" => $iconVersionColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => "#FFFF00",
								"transform" => "translate(6,0)",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,1.5)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-3.0-template",
												"transform" => "translate(-4.6,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-3.0-template",
												"transform" => "translate(-2.3,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-3.0-template",
												"transform" => "translate(0,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-3.0-template",
												"transform" => "translate(2.3,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-3.0-template",
												"transform" => "translate(4.6,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"fill" => "#FFFF00",
										"transform" => "translate(0,2.25)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-template",
												"transform" => "translate(-4,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-template",
												"transform" => "translate(-1.2,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-template",
												"transform" => "translate(1.2,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#pin-template",
												"transform" => "translate(4,0)",
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "direction-template",
						"points" => "-1.5,-1.5 1.5,-1.5 0.5,-0.5 -0.5,-0.5",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -1.5,
								"y" => -1.5,
								"width" => 3,
								"height" => 3,
								"fill" => "#AAAA60",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#direction-template",
								"fill" => "#888860",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(90)",
								"xlink:href" => "#direction-template",
								"fill" => "#777760",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(180)",
								"xlink:href" => "#direction-template",
								"fill" => "#999960",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(-90)",
								"xlink:href" => "#direction-template",
								"fill" => "#BBBB60",
							),
						),
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "icon-pin-3.0-template",
						"x" => $iconPinHeight / -2 * $breadboardMagnify,
						"y" => $iconPinHeight / -2 * $breadboardMagnify,
						"width" => $iconPinHeight * $breadboardMagnify,
						"height" => $iconPinHeight * $breadboardMagnify,
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "icon-pin-template",
						"x" => $iconPinWidth / -2 * $breadboardMagnify,
						"y" => $iconPinHeight / -2 * $breadboardMagnify,
						"width" => $iconPinWidth * $breadboardMagnify,
						"height" => $iconPinHeight * $breadboardMagnify,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => "#004400",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $globalInterval / 2, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / 2),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-pin",
												"transform" => "translate(0,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-P.GND-pin",
												"transform" => "translate(10,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-pin",
												"transform" => "translate(20,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--pin",
												"transform" => "translate(40,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC-pin",
												"transform" => "translate(50,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-pin",
												"transform" => "translate(60,0)",
												"xlink:href" => "#pin-template",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / -2),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-pin",
												"transform" => "translate(60,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-pin",
												"transform" => "translate(50,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--pin",
												"transform" => "translate(40,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-S.GND-pin",
												"transform" => "translate(30,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-pin",
												"transform" => "translate(20,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--pin",
												"transform" => "translate(10,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-pin",
												"transform" => "translate(0,0)",
												"xlink:href" => "#pin-template",
											),
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "#000000",
						"stroke-width" => 0.1 * $breadboardMagnify,
						"transform" => sprintf("translate(%s,%s)", ($breadboardWidth - $iconWidth * $breadboardMagnify) / 2, ($breadboardHeight - $iconHeight * $breadboardMagnify) / 2),
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth * $breadboardMagnify,
								"height" => $iconHeight * $breadboardMagnify,
								"rx" => 0.5,
								"fill" => $breadboardShieldColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0.75 * $breadboardMagnify,
								"y" => 0.75 * $breadboardMagnify,
								"width" => 10.5 * $breadboardMagnify,
								"height" => 1.5 * $breadboardMagnify,
								"rx" => 0.2,
								"fill" => $breadboardVersionColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => "#FFFF00",
								"transform" => sprintf("translate(%s,0)", 6 * $breadboardMagnify),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", 1.5 * $breadboardMagnify),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-3.0-template",
												"transform" => sprintf("translate(%s,0)", -4.6 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-3.0-template",
												"transform" => sprintf("translate(%s,0)", -2.3 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-3.0-template",
												"transform" => sprintf("translate(%s,0)", 0 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-3.0-template",
												"transform" => sprintf("translate(%s,0)", 2.3 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-3.0-template",
												"transform" => sprintf("translate(%s,0)", 4.6 * $breadboardMagnify),
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", 2.25 * $breadboardMagnify),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-template",
												"transform" => sprintf("translate(%s,0)", -4 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-template",
												"transform" => sprintf("translate(%s,0)", -1.2 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-template",
												"transform" => sprintf("translate(%s,0)", 1.2 * $breadboardMagnify),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"xlink:href" => "#icon-pin-template",
												"transform" => sprintf("translate(%s,0)", 4 * $breadboardMagnify),
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $pcbMultiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $pcbMultiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"transform" => sprintf("translate(%s,0)", $pcbWidth / 2),
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "silkscreen",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"font-size" => $pcbTextSize,
								"font-family" => $globalFontFamily,
								"fill" => "#000000",
								"stroke" => "none",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,15)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"text-anchor" => "start",
												"x" => 46,
												"y" => -1,
											),
											"tags" => "RX-",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"text-anchor" => "end",
												"x" => -46,
												"y" => -1,
											),
											"tags" => "TX+",
										),
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "start",
												"transform" => "translate(-4,1)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -22,
														"transform" => "rotate(90)",
													),
													"tags" => "RX+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 0,
														"transform" => "rotate(90)",
													),
													"tags" => "S.GND",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 22,
														"transform" => "rotate(90)",
													),
													"tags" => "TX-",
												),
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"text-anchor" => "start",
										"transform" => "translate(-4,31)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 35,
												"transform" => "rotate(90)",
											),
											"tags" => "VCC",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 10,
												"transform" => "rotate(90)",
											),
											"tags" => "D-",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => -10,
												"transform" => "rotate(90)",
											),
											"tags" => "D+",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => -35,
												"transform" => "rotate(90)",
											),
											"tags" => "GND",
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"text-anchor" => "middle",
										"transform" => sprintf("translate(0,%s)", $pcbHeight / 2 + $pcbTitleSize * 2),
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 0,
											),
											"tags" => "USB (3.0)",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => $pcbTitleSize,
											),
											"tags" => "Type-A",
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper1",
						"class" => "top-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"id" => "copper0",
								"class" => "bottom-layer",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"opacity" => 0,
										"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
									),
									"tags" => array(
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinThickness,
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"id" => "1st-pin-template",
											),
											"tags" => array(
												array(
													"name" => "rect",
													"attributes" => array(
														"x" => -$pcbPinRadius,
														"y" => -$pcbPinRadius,
														"width" => $pcbPinRadius * 2,
														"height" => $pcbPinRadius * 2,
														"fill" => $pcbBackgroundColor,
														"stroke" => $pcbPinColor,
														"stroke-width" => $pcbPinThickness,
													),
												),
												array(
													"name" => "circle",
													"attributes" => array(
														"cx" => 0,
														"cy" => 0,
														"r" => $pcbPinRadius,
														"fill" => $pcbBackgroundColor,
														"stroke" => $pcbPinColor,
														"stroke-width" => $pcbPinThickness,
													),
												),
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "shield-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbShieldRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinThickness,
											),
										),
									),
								),
								array(
									"name" => "g",
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,25)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-VCC-pin",
														"xlink:href" => "#1st-pin-template",
														"transform" => "translate(-35,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-D--pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(-10,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-D+-pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(10,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-P.GND-pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(35,0)",
													),
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,10)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-RX--pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(40,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-RX+-pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(20,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-S.GND-pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(0,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-TX--pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(-20,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-TX+-pin",
														"xlink:href" => "#pin-template",
														"transform" => "translate(-40,0)",
													),
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,40)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SBL.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(-65,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SBR.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(65,0)",
													),
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,95)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SFL.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(-65,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SFR.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(65,0)",
													),
												),
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $schematicTextSize,
						"font-family" => $globalFontFamily,
						"text-anchor" => "middle",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 20,
								"y" => 20,
								"width" => $schematicWidth - 40,
								"height" => $schematicHeight - 40,
								"fill" => $schematicBackgroundColor,
								"stroke" => $schematicBorderColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $schematicWidth / 2),
								"fill" => $schematicPinColor,
								"stroke" => $schematicPinColor,
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"stroke" => "none",
										"transform" => "translate(0,35)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 0,
											),
											"tags" => "USB (3.0)",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => $schematicTextSize,
											),
											"tags" => "Type-A",
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,30)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => -40,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SBL.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-40,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-50,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 40,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SBR.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(40,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(50,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,40)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => -40,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SFL.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-40,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-50,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 40,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SFR.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(40,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(50,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,50)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(16,10) rotate(90)",
											),
											"tags" => "VCC",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(15,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(15,20)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(6,10) rotate(90)",
											),
											"tags" => "D-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(5,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(5,20)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-8.75,10) rotate(90)",
											),
											"tags" => "D+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-5,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-5,20)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-18.75,10) rotate(90)",
											),
											"tags" => "P.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-P.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-15,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-P.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-15,20)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,0)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-23.75,10) rotate(90)",
											),
											"tags" => "RX-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-20,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-20,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-13.75,10) rotate(90)",
											),
											"tags" => "RX+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-10,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-10,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(1,10) rotate(90)",
											),
											"tags" => "S.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-S.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(0,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-S.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(0,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(11,10) rotate(90)",
											),
											"tags" => "TX-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(10,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(10,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(21,10) rotate(90)",
											),
											"tags" => "TX+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(20,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(20,0)",
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}