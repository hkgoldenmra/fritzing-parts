#!/bin/bash

for f in \
1pF 2.2pF 4.7pF 10pF 22pF 47pF 100pF 220pF 470pF \
1nF 2.2nF 4.7nF 10nF 22nF 47nF 100nF 220nF 470nF \
1uF 2.2uF 4.7uF 10uF 22uF 47uF 100uF 220uF 470uF \
1mF 2.2mF 4.7mF 10mF 22mF 47mF 100mF 220mF 470mF \
1F; do

for v in \
4V 6V 10V 16V 25V 50V 100V 250V 500V \
; do

F="${f}"
V="${v}"
# Do not modify the code below
PREFIX="electrolytic-capacitor"

ICON="icon"
BREADBOARD="breadboard"
SCHEMATIC="schematic"
PCB="pcb"
PART="part"
DIST="dist"

mkdir "${ICON}"
mkdir "${BREADBOARD}"
mkdir "${SCHEMATIC}"
mkdir "${PCB}"
mkdir "${PART}"
mkdir "${DIST}"

UUID=`uuidgen`
part=`cat "${PART}.${PREFIX}-template.fzp"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{UUID\\\\}/${UUID}/g"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{F\\\\}/${F}/g"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{V\\\\}/${V}/g"`

breadboard=`cat "svg.${BREADBOARD}.${PREFIX}-template.svg"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{F\\\\}/${F}/g"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{V\\\\}/${V}/g"`

FILENAME=`echo "${PREFIX}-${F}-${V}" | tr "[[:upper:]]" "[[:lower:]]"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{FILENAME\\\\}/${FILENAME}/g"`

cp "svg.${ICON}.${PREFIX}-template.svg" "${ICON}/${FILENAME}.svg"
echo -n "${breadboard}" >"${BREADBOARD}/${FILENAME}.svg"
cp "svg.${SCHEMATIC}.${PREFIX}-template.svg" "${SCHEMATIC}/${FILENAME}.svg"
cp "svg.${PCB}.${PREFIX}-template.svg" "${PCB}/${FILENAME}.svg"
echo -n "${part}" >"${PART}/${FILENAME}.fzp"

ln -s "${ICON}/${FILENAME}.svg" "svg.${ICON}.${FILENAME}.svg"
ln -s "${BREADBOARD}/${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg"
ln -s "${SCHEMATIC}/${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg"
ln -s "${PCB}/${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg"
ln -s "${PART}/${FILENAME}.fzp" "${PART}.${FILENAME}.fzp"

zip \
"${DIST}/${FILENAME}.fzpz" \
"svg.${ICON}.${FILENAME}.svg" \
"svg.${BREADBOARD}.${FILENAME}.svg" \
"svg.${SCHEMATIC}.${FILENAME}.svg" \
"svg.${PCB}.${FILENAME}.svg" \
"${PART}.${FILENAME}.fzp" \

rm "svg.${ICON}.${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg" "${PART}.${FILENAME}.fzp"
done
done