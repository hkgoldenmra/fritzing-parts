<?php

$globalRows = 1;
$globalColumns = 1;
$globalType = true;
$globalTitle = sprintf("Pin Header %sx%s %s", $globalRows, $globalColumns, (($globalType) ? "Male" : "Female"));
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "PIN";
$fritzingProperties = array(
	"columns" => $globalColumns,
	"family" => "A Pin Header",
	"package" => "THT",
	"rows" => $globalRows,
	"type" => (($globalType) ? "Male" : "Female"),
);
$fritzingTags = array("Pin Header", $fritzingProperties["package"]);

// icon settings
$iconUnit = "in";
$iconBackgroundColor = "#404040";
$iconPinColor = "#888866";
$iconPinTopColor = "#BBBB88";
$iconPinBottomColor = "#666644";
$iconPinLeftColor = "#999966";
$iconPinRightColor = "#777766";
$iconHoleColor = "#000000";
$iconHoleTopColor = "#222222";
$iconHoleBottomColor = "#555555";
$iconHoleLeftColor = "#333333";
$iconHoleRightColor = "#444444";

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardBackgroundColor = $iconBackgroundColor;
$breadboardPinColor = $iconPinColor;
$breadboardPinTopColor = $iconPinTopColor;
$breadboardPinBottomColor = $iconPinBottomColor;
$breadboardPinLeftColor = $iconPinLeftColor;
$breadboardPinRightColor = $iconPinRightColor;
$breadboardHoleColor = $iconHoleColor;
$breadboardHoleTopColor = $iconHoleTopColor;
$breadboardHoleBottomColor = $iconHoleBottomColor;
$breadboardHoleLeftColor = $iconHoleLeftColor;
$breadboardHoleRightColor = $iconHoleRightColor;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;

// Don't change the code below, unless you know what to do...
function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = $globalInterval;
$iconHeight = $globalInterval;
$breadboardWidth = $globalColumns * $globalInterval;
$breadboardHeight = $globalRows * $globalInterval;
$pcbWidth = $breadboardWidth;
$pcbHeight = $breadboardHeight;
$schematicWidth = 30;
$schematicHeight = $globalColumns * $globalRows * $globalInterval;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("pin-header-%sx%s-%s", $globalRows, $globalColumns, (($globalType) ? "Male" : "Female"));
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
for ($fritzingI = 0; $fritzingI < $globalRows; $fritzingI++) {
	for ($fritzingJ = 0; $fritzingJ < $globalColumns; $fritzingJ++) {
		$name = sprintf("R%s-C%s", $fritzingI, $fritzingJ);
		$id = sprintf("connector-%s", $name);
		array_push($fritzingConnectorsChain, array(
			"name" => "connector",
			"attributes" => array(
				"id" => $id,
				"name" => $name,
				"type" => "male",
			),
			"tags" => array(
				array(
					"name" => "description",
					"tags" => $name,
				),
				array(
					"name" => "views",
					"tags" => array(
						array(
							"name" => "breadboardView",
							"tags" => array(
								array(
									"name" => "p",
									"attributes" => array(
										"svgId" => sprintf("%s-pin", $id),
//										"terminalId" => sprintf("%s-terminal", $id),
//										"legId" => sprintf("%s-leg", $id),
										"layer" => "breadboard",
									),
								),
							),
						),
						array(
							"name" => "schematicView",
							"tags" => array(
								array(
									"name" => "p",
									"attributes" => array(
										"svgId" => sprintf("%s-pin", $id),
										"terminalId" => sprintf("%s-terminal", $id),
//										"legId" => sprintf("%s-leg", $id),
										"layer" => "schematic",
									),
								),
							),
						),
						array(
							"name" => "pcbView",
							"tags" => array(
								array(
									"name" => "p",
									"attributes" => array(
										"svgId" => sprintf("%s-pin", $id),
//										"terminalId" => sprintf("%s-terminal", $id),
//										"legId" => sprintf("%s-leg", $id),
										"layer" => "copper1",
									),
								),
								array(
									"name" => "p",
									"attributes" => array(
										"svgId" => sprintf("%s-pin", $id),
//										"terminalId" => sprintf("%s-terminal", $id),
//										"legId" => sprintf("%s-leg", $id),
										"layer" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		));
	}
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "pin-direction-template",
						"points" => "-1.5,-1.5 1.5,-1.5 0.5,-0.5 -0.5,-0.5",
					),
				),
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "hole-direction-template",
						"points" => "-3,-3 3,-3 1.5,-1.5 -1.5,-1.5",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "male-pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-2,-5 2,-5 5,-2 5,2 2,5 -2,5 -5,2 -5,-2",
								"fill" => $iconBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -1.5,
								"y" => -1.5,
								"width" => 3,
								"height" => 3,
								"fill" => $iconPinColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $iconPinTopColor,
								"transform" => "rotate(0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $iconPinBottomColor,
								"transform" => "rotate(180)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $iconPinLeftColor,
								"transform" => "rotate(-90)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $iconPinRightColor,
								"transform" => "rotate(90)",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "female-pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -5,
								"y" => -5,
								"width" => 10,
								"height" => 10,
								"fill" => $iconBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -3,
								"y" => -3,
								"width" => 6,
								"height" => 6,
								"fill" => $iconHoleColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $iconHoleTopColor,
								"transform" => "rotate(0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $iconHoleBottomColor,
								"transform" => "rotate(180)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $iconHoleLeftColor,
								"transform" => "rotate(-90)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $iconHoleRightColor,
								"transform" => "rotate(90)",
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => (($globalType) ? "#male-pin-template" : "#female-pin-template"),
								"transform" => "translate(5,5)",
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardPinsChain = array();
$breadboardTerminalsChain = array();
for ($breadboardI = 0; $breadboardI < $globalRows; $breadboardI++) {
	for ($breadboardJ = 0; $breadboardJ < $globalColumns; $breadboardJ++) {
		$connector = sprintf("connector-R%s-C%s", $breadboardI, $breadboardJ);
		$translate = sprintf("translate(%s,%s)", $breadboardJ * $globalInterval, $breadboardI * -$globalInterval);
		array_push($breadboardPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"xlink:href" => (($globalType) ? "#male-pin-template" : "#female-pin-template"),
				"transform" => $translate,
			),
		));
		array_push($breadboardTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("%s-pin", $connector),
				"xlink:href" => (($breadboardI > 0 || $breadboardJ > 0) ? "#pin-template" : "#1st-pin-template"),
				"transform" => $translate,
			),
		));
	}
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "pin-direction-template",
						"points" => "-1.5,-1.5 1.5,-1.5 0.5,-0.5 -0.5,-0.5",
					),
				),
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "hole-direction-template",
						"points" => "-3,-3 3,-3 1.5,-1.5 -1.5,-1.5",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "male-pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-2,-5 2,-5 5,-2 5,2 2,5 -2,5 -5,2 -5,-2",
								"fill" => $breadboardBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -1.5,
								"y" => -1.5,
								"width" => 3,
								"height" => 3,
								"fill" => $breadboardPinColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $breadboardPinTopColor,
								"transform" => "rotate(0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $breadboardPinBottomColor,
								"transform" => "rotate(180)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $breadboardPinLeftColor,
								"transform" => "rotate(-90)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-direction-template",
								"fill" => $breadboardPinRightColor,
								"transform" => "rotate(90)",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "female-pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -5,
								"y" => -5,
								"width" => 10,
								"height" => 10,
								"fill" => $breadboardBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -3,
								"y" => -3,
								"width" => 6,
								"height" => 6,
								"fill" => $breadboardHoleColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $breadboardHoleTopColor,
								"transform" => "rotate(0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $breadboardHoleBottomColor,
								"transform" => "rotate(180)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $breadboardHoleLeftColor,
								"transform" => "rotate(-90)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#hole-direction-template",
								"fill" => $breadboardHoleRightColor,
								"transform" => "rotate(90)",
							),
						),
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "1st-pin-template",
						"x" => -2.5,
						"y" => -2.5,
						"width" => 5,
						"height" => 5,
						"opacity" => 0,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 2.5,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(5,%s)", $breadboardHeight - 5),
							),
							"tags" => array(
								array(
									"name" => "g",
									"tags" => $breadboardPinsChain,
								),
								array(
									"name" => "g",
									"tags" => $breadboardTerminalsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbPinsChain = array();
for ($pcbI = 0; $pcbI < $globalRows; $pcbI++) {
	for ($pcbJ = 0; $pcbJ < $globalColumns; $pcbJ++) {
		array_push($pcbPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-R%s-C%s-pin", $pcbI, $pcbJ),
				"xlink:href" => (($pcbI > 0 || $pcbJ > 0) ? "#pin-template" : "#1st-pin-template"),
				"transform" => sprintf("translate(%s,%s)", $pcbJ * $globalInterval, $pcbI * -$globalInterval),
			),
		));
	}
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 0,
						"y" => 0,
						"width" => $pcbWidth,
						"height" => $pcbHeight,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => $pcbBackgroundColor,
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(5,%s)", $pcbHeight - 5),
							),
							"tags" => $pcbPinsChain,
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicDirectionsChain = array();
$schematicPinsChain = array();
$schematicTerminalsChain = array();
for ($schematicI = 0; $schematicI < $globalRows; $schematicI++) {
	for ($schematicJ = 0; $schematicJ < $globalColumns; $schematicJ++) {
		$connector = sprintf("connector-R%s-C%s", $schematicI, $schematicJ);
		$translate = sprintf("translate(0,%s)", ($schematicI * $globalColumns + $schematicJ) * $globalInterval);
		array_push($schematicDirectionsChain, array(
			"name" => "use",
			"attributes" => array(
				"xlink:href" => (($globalType) ? "#male-direction-template" : "#female-direction-template"),
				"transform" => $translate,
			),
		));
		array_push($schematicPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("%s-pin", $connector),
				"xlink:href" => "#pin-template",
				"transform" => $translate,
			),
		));
		array_push($schematicTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("%s-terminal", $connector),
				"xlink:href" => "#terminal-template",
				"transform" => $translate,
			),
		));
	}
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "male-direction-template",
						"points" => "0,-5 10,0 0,5 10,0 0,0",
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "female-direction-template",
						"points" => "10,-5 0,0 10,5",
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "0,0 20,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,5)",
					),
					"tags" => $schematicDirectionsChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(0,5)",
					),
					"tags" => $schematicPinsChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(0,5)",
					),
					"tags" => $schematicTerminalsChain,
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}