#!/bin/bash
asdc="0"
while [ "${asdc}" -lt "60" ]; do
asdc=`echo "${asdc} + 1" | bc`
COLUMNS="${asdc}"
# Do not modify the code below
PREFIX="dip-switch"

ICON="icon"
BREADBOARD="breadboard"
SCHEMATIC="schematic"
PCB="pcb"
PART="part"
DIST="dist"

mkdir "${ICON}"
mkdir "${BREADBOARD}"
mkdir "${SCHEMATIC}"
mkdir "${PCB}"
mkdir "${PART}"
mkdir "${DIST}"

UUID=`uuidgen`
part=`cat "${PART}.${PREFIX}-template.fzp"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{UUID\\\\}/${UUID}/g"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{COLUMNS\\\\}/${COLUMNS}/g"`

BREADBOARD_BASE_SIZE="4.944"
BREADBOARD_BASE_WIDTH="7.176"
BREADBOARD_COLUMN_WIDTH="7.2"
BREADBOARD_WIDTH=`echo "${BREADBOARD_BASE_SIZE} + ${BREADBOARD_BASE_WIDTH} * ${COLUMNS}" | bc`
BREADBOARD_HALF_WIDTH=`echo "${BREADBOARD_WIDTH} / 2" | bc`
breadboard=`cat "svg.${BREADBOARD}.${PREFIX}-template.svg"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{WIDTH\\\\}/${BREADBOARD_WIDTH}/g"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{HALF_WIDTH\\\\}/${BREADBOARD_HALF_WIDTH}/g"`

SCHEMATIC_BASE_SIZE="0.576"
SCHEMATIC_COLUMN_SIZE="4.176"
SCHEMATIC_COLUMN_WIDTH="7.2"
SCHEMATIC_WIDTH=`echo "${SCHEMATIC_COLUMN_SIZE} * 2 + ${SCHEMATIC_COLUMN_WIDTH} * ${COLUMNS}" | bc`
SCHEMATIC_INNER_WIDTH=`echo "${SCHEMATIC_WIDTH} - 2 * ${SCHEMATIC_BASE_SIZE}" | bc`
SCHEMATIC_HALF_WIDTH=`echo "${SCHEMATIC_WIDTH} / 2" | bc`
schematic=`cat "svg.${SCHEMATIC}.${PREFIX}-template.svg"`
schematic=`echo "${schematic}" | sed -r "s/\\\\$\\\\{HEIGHT\\\\}/${SCHEMATIC_WIDTH}/g"`
schematic=`echo "${schematic}" | sed -r "s/\\\\$\\\\{INNER_HEIGHT\\\\}/${SCHEMATIC_INNER_WIDTH}/g"`
schematic=`echo "${schematic}" | sed -r "s/\\\\$\\\\{HALF_HEIGHT\\\\}/${SCHEMATIC_HALF_WIDTH}/g"`

PCB_COLUMN_SIZE="10"
PCB_WIDTH=`echo "${PCB_COLUMN_SIZE} * ${COLUMNS}" | bc`
PCB_INNER_WIDTH=`echo "${PCB_WIDTH}" | bc`
PCB_INCH_WIDTH=`echo "${PCB_WIDTH} * 0.01" | bc`
pcb=`cat "svg.${PCB}.${PREFIX}-template.svg"`
pcb=`echo "${pcb}" | sed -r "s/\\\\$\\\\{WIDTH\\\\}/${PCB_WIDTH}/g"`
pcb=`echo "${pcb}" | sed -r "s/\\\\$\\\\{INNER_WIDTH\\\\}/${PCB_INNER_WIDTH}/g"`
pcb=`echo "${pcb}" | sed -r "s/\\\\$\\\\{INCH_WIDTH\\\\}/${PCB_INCH_WIDTH}/g"`

part_g_start=""
part_g_end=""
breadboard_g_column=""
breadboard_g_start_pin=""
breadboard_g_end_pin=""
pcb_g_column=""
pcb_g_start_pin=""
pcb_g_end_pin=""
schematic_g_column=""
schematic_g_start_pin=""
schematic_g_end_pin=""
i="0"
while [ "${i}" -lt "${COLUMNS}" ]; do
	I=`echo "${i} + 1" | bc`
	j=`echo "${COLUMNS} + ${i}" | bc`
	J=`echo "${j} + 1" | bc`

	CX=`echo "${BREADBOARD_COLUMN_WIDTH} * ${i}" | bc`
	tg=`cat "${PART}.${PREFIX}-template-g.fzp"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${i}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${I}/g"`
	part_g_start="${part_g_start}${tg}"

	tg=`cat "${PART}.${PREFIX}-template-g.fzp"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${j}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${J}/g"`
	part_g_end="${part_g_end}${tg}"

	tg=`cat "svg.${BREADBOARD}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${i}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{CX\\\\}/${CX}/g"`
	breadboard_g_start_pin="${breadboard_g_start_pin}${tg}"

	CX=`echo "${PCB_COLUMN_SIZE} * ${i}" | bc`
	tg=`cat "svg.${PCB}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${i}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{CX\\\\}/${CX}/g"`
	pcb_g_start_pin="${pcb_g_start_pin}${tg}"

	SIDE=""
	Y=`echo "${SCHEMATIC_COLUMN_WIDTH} * ${i}" | bc`
	tg=`cat "svg.${SCHEMATIC}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${i}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${I}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{Y\\\\}/${Y}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{SIDE\\\\}/${SIDE}/g"`
	schematic_g_start_pin="${schematic_g_start_pin}${tg}"

	i=`echo "${i} + 1" | bc`
	CX=`echo "${BREADBOARD_COLUMN_WIDTH} * (${COLUMNS} - ${i})" | bc`
	tg=`cat "svg.${BREADBOARD}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${j}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{CX\\\\}/${CX}/g"`
	breadboard_g_end_pin="${breadboard_g_end_pin}${tg}"

	CX=`echo "${PCB_COLUMN_SIZE} * (${COLUMNS} - ${i})" | bc`
	tg=`cat "svg.${PCB}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${j}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{CX\\\\}/${CX}/g"`
	pcb_g_end_pin="${pcb_g_end_pin}${tg}"

	X=`echo "${BREADBOARD_COLUMN_WIDTH} * (${i} - 1)" | bc`
	tg=`cat "svg.${BREADBOARD}.${PREFIX}-template-g-column.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{X\\\\}/${X}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${i}/g"`
	breadboard_g_column="${breadboard_g_column}${tg}"

	X=`echo "${PCB_COLUMN_SIZE} * (${i} - 1)" | bc`
	tg=`cat "svg.${PCB}.${PREFIX}-template-g-column.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{X\\\\}/${X}/g"`
	pcb_g_column="${pcb_g_column}${tg}"

	SIDE="-"
	Y=`echo "${SCHEMATIC_COLUMN_WIDTH} * (${COLUMNS} - ${i})" | bc`
	tg=`cat "svg.${SCHEMATIC}.${PREFIX}-template-g-pin.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{0_BASED_INDEX\\\\}/${j}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${J}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{Y\\\\}/${Y}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{SIDE\\\\}/${SIDE}/g"`
	schematic_g_end_pin="${schematic_g_end_pin}${tg}"

	Y=`echo "${SCHEMATIC_COLUMN_WIDTH} * (${i} - 1)" | bc`
	tg=`cat "svg.${SCHEMATIC}.${PREFIX}-template-g-column.svg"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{Y\\\\}/${Y}/g"`
	tg=`echo "${tg}" | sed -r "s/\\\\$\\\\{1_BASED_INDEX\\\\}/${i}/g"`
	schematic_g_column="${schematic_g_column}${tg}"
done
breadboard="${breadboard/\$\{CHAIN_COLUMN\}/${breadboard_g_column}}"
breadboard="${breadboard/\$\{CHAIN_START_PIN\}/${breadboard_g_start_pin}}"
breadboard="${breadboard/\$\{CHAIN_END_PIN\}/${breadboard_g_end_pin}}"
pcb="${pcb/\$\{CHAIN_COLUMN\}/${pcb_g_column}}"
pcb="${pcb/\$\{CHAIN_START_PIN\}/${pcb_g_start_pin}}"
pcb="${pcb/\$\{CHAIN_END_PIN\}/${pcb_g_end_pin}}"
schematic="${schematic/\$\{CHAIN_COLUMN\}/${schematic_g_column}}"
schematic="${schematic/\$\{CHAIN_START_PIN\}/${schematic_g_start_pin}}"
schematic="${schematic/\$\{CHAIN_END_PIN\}/${schematic_g_end_pin}}"

FILENAME=`echo "${PREFIX}-${COLUMNS}" | tr "[[:upper:]]" "[[:lower:]]"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{FILENAME\\\\}/${FILENAME}/g"`

cp "svg.${ICON}.${PREFIX}-template.svg" "${ICON}/${FILENAME}.svg"
echo -n "${breadboard}" >"${BREADBOARD}/${FILENAME}.svg"
echo -n "${schematic}" >"${SCHEMATIC}/${FILENAME}.svg"
echo -n "${pcb}" >"${PCB}/${FILENAME}.svg"
echo -n "${part/\$\{CHAIN\}/${part_g_start}${part_g_end}}" >"${PART}/${FILENAME}.fzp"

ln -s "${ICON}/${FILENAME}.svg" "svg.${ICON}.${FILENAME}.svg"
ln -s "${BREADBOARD}/${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg"
ln -s "${SCHEMATIC}/${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg"
ln -s "${PCB}/${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg"
ln -s "${PART}/${FILENAME}.fzp" "${PART}.${FILENAME}.fzp"

zip \
"${DIST}/${FILENAME}.fzpz" \
"svg.${ICON}.${FILENAME}.svg" \
"svg.${BREADBOARD}.${FILENAME}.svg" \
"svg.${SCHEMATIC}.${FILENAME}.svg" \
"svg.${PCB}.${FILENAME}.svg" \
"${PART}.${FILENAME}.fzp" \

rm "svg.${ICON}.${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg" "${PART}.${FILENAME}.fzp"
done