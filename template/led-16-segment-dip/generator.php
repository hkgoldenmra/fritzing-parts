<?php

// global settings
$globalColor = "Red";
$globalPins = array(
	"R" => array(10 => "1"),
	"C" => array(0 => "AL", 17 => "AR", 15 => "B", 12 => "C", 9 => "DR", 8 => "DL", 7 => "E", 3 => "F", 4 => "GL", 14 => "GR", 1 => "UP", 5 => "DN", 16 => "UPR", 6 => "DNL", 2 => "UPL", 13 => "DNR", 11 => "P"),
	"E" => array(),
//	"R" => array(9 => "1", 3 => "2"),
//	"C" => array(12 => "AL", 10 => "AR", 16 => "B", 7 => "C", 8 => "DR", 0 => "DL", 5 => "E", 14 => "F", 19 => "GL", 15 => "GR", 17 => "UP", 2 => "DN", 11 => "UPR", 1 => "DNL", 18 => "UPL", 6 => "DNR", 4 => "P"),
//	"E" => array(13 => "NC"),
//	"R" => array(13 => "1", 9 => "2", 3 => "3"),
//	"C" => array(12 => "AL", 10 => "AR", 16 => "B", 7 => "C", 8 => "DR", 0 => "DL", 5 => "E", 14 => "F", 19 => "GL", 15 => "GR", 17 => "UP", 2 => "DN", 11 => "UPR", 1 => "DNL", 18 => "UPL", 6 => "DNR", 4 => "P"),
//	"E" => array(),
//	"R" => array(10 => "1", 14 => "2", 17 => "3", 0 => "4"),
//	"C" => array(12 => "AL", 13 => "AR", 9 => "B", 7 => "C", 8 => "DR", 3 => "DL", 5 => "E", 20 => "F", 19 => "GL", 15 => "GR", 16 => "UP", 2 => "DN", 11 => "UPR", 1 => "DNL", 18 => "UPL", 6 => "DNR", 4 => "P"),
//	"E" => array(21 => "NC"),
);
$globalDirection = true;
$globalTitle = sprintf("LED 16 Segment DIP %s Digit %s %s", count($globalPins["R"]), $globalColor, (($globalDirection) ? "Anode" : "Cathode"));
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "16SEG";
$fritzingTags = array("LED", "16SEG", "DIP");
$fritzingProperties = array(
	"color" => $globalColor,
	"digits" => count($globalPins["R"]),
	"common" => (($globalDirection) ? "Anode" : "Cathode"),
	"family" => "A LED 16 Segment",
	"package" => "DIP",
);

// icon settings
$iconUnit = "in";
$iconShieldColor = "#000000";
$iconLEDColor = "#888888";

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardShieldColor = $iconShieldColor;
$breadboardLEDColor = $iconLEDColor;
$breadboardPinRadius = 2.5;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTextSize = 4;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 8;
$schematicTextColor = $schematicPinColor;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 4;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 110;
$iconHeight = 110;
$breadboardWidth = count($globalPins["R"]) * 90;
$breadboardHeight = $iconHeight;
$pcbWidth = $breadboardWidth;
$pcbHeight = $breadboardHeight;
$schematicWidth = count($globalPins["C"]) * 30 + 5;
$schematicHeight = count($globalPins["R"]) * 30 + 5;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("led-16-segment-dip-%s-digit-%s-%s", count($globalPins["R"]), $globalColor, (($globalDirection) ? "Anode" : "Cathode"));
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
$fritzingPins = array();
foreach ($globalPins as $key => $array) {
	foreach ($array as $index => $value) {
		$fritzingPins[$index] = array("name" => $value, "direction" => (($key == "R" || $key == "E") && $globalDirection) || ($key == "C" && !$globalDirection));
	}
}
ksort($fritzingPins);
foreach ($fritzingPins as $value) {
	$id = sprintf("connector-%s", $value["name"]);
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "16-segment-template",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $globalColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 50.547446,22.315888 c 0.310428,0 0.577827,0.129089 0.76224,0.335017 l 5.295723,-4.791662 C 55.32374,16.42697 53.445802,15.541788 51.263582,15.541788 H 36.117136 l -0.70999,6.7741 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 44.925926,86.556123 c -0.03074,0.301208 -0.19056,0.587048 -0.418003,0.792976 l 4.290674,4.791662 c 1.582877,-1.4292 2.67706,-3.408565 2.904503,-5.581564 L 54.666,58.393199 51.635482,55.003076 47.8919,58.393199 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 43.674991,87.690263 H 28.534692 l -0.70999,6.76488 h 15.137226 c 2.182219,0 4.256865,-0.882109 5.833595,-2.317456 l -4.290674,-4.791662 c -0.224369,0.205928 -0.522503,0.344238 -0.829858,0.344238 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 12.380122,86.556123 15.336875,58.390126 12.306356,55.000002 8.5627744,58.390126 5.6029473,86.556123 c -0.2305161,2.172999 0.4487381,4.155438 1.7304078,5.581564 L 12.629079,87.346025 C 12.44774,87.143171 12.346313,86.854258 12.380122,86.556123 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 12.380122,86.556123 15.336875,58.390126 12.306356,55.000002 8.5627744,58.390126 5.6029473,86.556123 c -0.2305161,2.172999 0.4487381,4.155438 1.7304078,5.581564 L 12.629079,87.346025 C 12.44774,87.143171 12.346313,86.854258 12.380122,86.556123 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 19.012839,23.446954 c 0.03073,-0.307355 0.193634,-0.587048 0.421076,-0.796049 l -4.290673,-4.791662 c -1.582878,1.432274 -2.677061,3.408565 -2.907577,5.587711 l -2.9567538,28.165998 3.0305188,3.38705 3.743582,-3.38705 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 31.614387,58.387052 H 47.8919 l 3.740508,-3.38705 -3.033592,-3.38705 H 32.327451 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 38.791123,22.315888 35.714501,51.612952 H 28.9404 l 3.079696,-29.297064 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 35.001438,58.387052 -3.079696,29.303211 h -6.7741 l 3.079695,-29.303211 z",
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $iconLEDColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 20.263773,22.315888 h 15.1403 l 0.709989,-6.7741 H 20.970689 c -2.176072,0 -4.247644,0.888255 -5.830521,2.317455 l 4.290674,4.791662 c 0.230516,-0.202854 0.522503,-0.335017 0.832931,-0.335017 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 58.335817,23.446954 c 0.230516,-2.179146 -0.448738,-4.155437 -1.730408,-5.587711 l -5.295723,4.791662 c 0.187486,0.205928 0.282766,0.488694 0.248957,0.796049 l -2.959827,28.165998 3.033592,3.38705 3.743582,-3.38705 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 13.388245,87.690263 c -0.304281,0 -0.574753,-0.13831 -0.759166,-0.341164 l -5.2957239,4.791662 c 1.2816697,1.435347 3.1657549,2.317455 5.3449009,2.317455 h 15.146446 l 0.70999,-6.76488 H 13.388245 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 16.053012,51.612952 -3.746656,3.38705 3.030519,3.38705 h 16.277512 l 0.713064,-6.7741 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 36.814831,41.110637 35.714501,51.612952 50.575108,32.802835 51.681585,22.300521 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 27.123933,68.886294 28.227337,58.387052 13.363657,77.197169 12.260253,87.69641 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 30.043804,41.110637 28.9404,51.612952 18.029303,32.802835 19.132707,22.300521 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 33.901107,68.886294 35.001438,58.387052 45.909461,77.197169 44.812204,87.69641 Z",
									),
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => 60.514961,
										"cy" => 90.576324,
										"r" => 3.8818917,
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $globalInterval * 2),
								"xlink:href" => "#16-segment-template",
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardTemplatesChain = array();
$breadboardFrontPinsChain = array();
$breadboardBackPinsChain = array();
for ($breadboardI = 0; $breadboardI < count($globalPins["R"]); $breadboardI++) {
	array_push($breadboardTemplatesChain, array(
		"name" => "use",
		"attributes" => array(
			"transform" => sprintf("translate(%s,0)", $breadboardI * $breadboardWidth / count($globalPins["R"])),
			"xlink:href" => "#16-segment-template",
		),
	));
}
$breadboardI = 0;
foreach ($fritzingPins as $value) {
	if ($breadboardI < count($fritzingPins) / 2) {
		array_push($breadboardFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), $breadboardI * $globalInterval),
				"xlink:href" => (($breadboardI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		array_push($breadboardBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), (count($fritzingPins) - $breadboardI - 1) * $globalInterval),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$breadboardI++;
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $breadboardPinRadius,
						"opacity" => 0,
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "1st-pin-template",
						"x" => -$breadboardPinRadius,
						"y" => -$breadboardPinRadius,
						"width" => $breadboardPinRadius * 2,
						"height" => $breadboardPinRadius * 2,
						"opacity" => 0,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "16-segment-template",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $globalColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 50.547446,22.315888 c 0.310428,0 0.577827,0.129089 0.76224,0.335017 l 5.295723,-4.791662 C 55.32374,16.42697 53.445802,15.541788 51.263582,15.541788 H 36.117136 l -0.70999,6.7741 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 44.925926,86.556123 c -0.03074,0.301208 -0.19056,0.587048 -0.418003,0.792976 l 4.290674,4.791662 c 1.582877,-1.4292 2.67706,-3.408565 2.904503,-5.581564 L 54.666,58.393199 51.635482,55.003076 47.8919,58.393199 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 43.674991,87.690263 H 28.534692 l -0.70999,6.76488 h 15.137226 c 2.182219,0 4.256865,-0.882109 5.833595,-2.317456 l -4.290674,-4.791662 c -0.224369,0.205928 -0.522503,0.344238 -0.829858,0.344238 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 12.380122,86.556123 15.336875,58.390126 12.306356,55.000002 8.5627744,58.390126 5.6029473,86.556123 c -0.2305161,2.172999 0.4487381,4.155438 1.7304078,5.581564 L 12.629079,87.346025 C 12.44774,87.143171 12.346313,86.854258 12.380122,86.556123 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 12.380122,86.556123 15.336875,58.390126 12.306356,55.000002 8.5627744,58.390126 5.6029473,86.556123 c -0.2305161,2.172999 0.4487381,4.155438 1.7304078,5.581564 L 12.629079,87.346025 C 12.44774,87.143171 12.346313,86.854258 12.380122,86.556123 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 19.012839,23.446954 c 0.03073,-0.307355 0.193634,-0.587048 0.421076,-0.796049 l -4.290673,-4.791662 c -1.582878,1.432274 -2.677061,3.408565 -2.907577,5.587711 l -2.9567538,28.165998 3.0305188,3.38705 3.743582,-3.38705 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 31.614387,58.387052 H 47.8919 l 3.740508,-3.38705 -3.033592,-3.38705 H 32.327451 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 38.791123,22.315888 35.714501,51.612952 H 28.9404 l 3.079696,-29.297064 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 35.001438,58.387052 -3.079696,29.303211 h -6.7741 l 3.079695,-29.303211 z",
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $iconLEDColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 20.263773,22.315888 h 15.1403 l 0.709989,-6.7741 H 20.970689 c -2.176072,0 -4.247644,0.888255 -5.830521,2.317455 l 4.290674,4.791662 c 0.230516,-0.202854 0.522503,-0.335017 0.832931,-0.335017 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 58.335817,23.446954 c 0.230516,-2.179146 -0.448738,-4.155437 -1.730408,-5.587711 l -5.295723,4.791662 c 0.187486,0.205928 0.282766,0.488694 0.248957,0.796049 l -2.959827,28.165998 3.033592,3.38705 3.743582,-3.38705 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 13.388245,87.690263 c -0.304281,0 -0.574753,-0.13831 -0.759166,-0.341164 l -5.2957239,4.791662 c 1.2816697,1.435347 3.1657549,2.317455 5.3449009,2.317455 h 15.146446 l 0.70999,-6.76488 H 13.388245 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m 16.053012,51.612952 -3.746656,3.38705 3.030519,3.38705 h 16.277512 l 0.713064,-6.7741 z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 36.814831,41.110637 35.714501,51.612952 50.575108,32.802835 51.681585,22.300521 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 27.123933,68.886294 28.227337,58.387052 13.363657,77.197169 12.260253,87.69641 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 30.043804,41.110637 28.9404,51.612952 18.029303,32.802835 19.132707,22.300521 Z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "M 33.901107,68.886294 35.001438,58.387052 45.909461,77.197169 44.812204,87.69641 Z",
									),
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => 60.514961,
										"cy" => 90.576324,
										"r" => 3.8818917,
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardShieldColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $globalInterval),
							),
							"tags" => $breadboardTemplatesChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", ((count($globalPins["R"]) > 1) ? ($breadboardWidth + $globalInterval - count($fritzingPins) * $globalInterval / 2) / 2 : $pcbWidth / 2), ((count($globalPins["R"]) > 1) ? $pcbHeight / 2 : $globalInterval * 3 / 2)),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * 4) : sprintf("translate(%s,0)", $globalInterval * -3)),
									),
									"tags" => $breadboardFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * -4) : sprintf("translate(%s,0)", $globalInterval * 3)),
									),
									"tags" => $breadboardBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontTextsChain = array();
$pcbBackTextsChain = array();
$pcbFrontPinsChain = array();
$pcbBackPinsChain = array();
$pcbI = 0;
foreach ($fritzingPins as $value) {
	if ($pcbI < count($fritzingPins) / 2) {
		$pcbX = $pcbI * $globalInterval;
		array_push($pcbFrontTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), $pcbX),
			),
			"tags" => $value["name"],
		));
		array_push($pcbFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), $pcbX),
				"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		$pcbX = (count($fritzingPins) - $pcbI - 1) * $globalInterval;
		array_push($pcbBackTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), $pcbX),
			),
			"tags" => $value["name"],
		));
		array_push($pcbBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf(((count($globalPins["R"]) > 1) ? "translate(%s,0)" : "translate(0,%s)"), $pcbX),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$pcbI++;
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 0,
						"y" => 0,
						"width" => $pcbWidth,
						"height" => $pcbHeight,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $pcbTextSize,
						"font-family" => $globalFontFamily,
						"fill" => $pcbSilkscreenColor,
						"stroke" => "none",
						"transform" => sprintf("translate(%s,%s)", ((count($globalPins["R"]) > 1) ? ($breadboardWidth + $globalInterval - count($fritzingPins) * $globalInterval / 2) / 2 : $pcbWidth / 2), ((count($globalPins["R"]) > 1) ? $pcbHeight / 2 : $globalInterval * 3 / 2 + fontBaselineOffset($pcbTextSize))),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => ((count($globalPins["R"]) > 1) ? "middle" : "start"),
								"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * 7 / 2) : sprintf("translate(%s,0)", $globalInterval * 5 / -2)),
							),
							"tags" => $pcbFrontTextsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => ((count($globalPins["R"]) > 1) ? "middle" : "end"),
								"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * -3 - fontBaselineOffset($pcbTextSize)) : sprintf("translate(%s,0)", $globalInterval * 5 / 2)),
							),
							"tags" => $pcbBackTextsChain,
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => $pcbBackgroundColor,
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", ((count($globalPins["R"]) > 1) ? ($breadboardWidth + $globalInterval - count($fritzingPins) * $globalInterval / 2) / 2 : $pcbWidth / 2), ((count($globalPins["R"]) > 1) ? $pcbHeight / 2 : $globalInterval * 3 / 2)),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * 4) : sprintf("translate(%s,0)", $globalInterval * -3)),
									),
									"tags" => $pcbFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => ((count($globalPins["R"]) > 1) ? sprintf("translate(0,%s)", $globalInterval * -4) : sprintf("translate(%s,0)", $globalInterval * 3)),
									),
									"tags" => $pcbBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicColumnLinesChain = array();
$schematicRowLinesChain = array();
$schematicJointsColumnChain = array();
$schematicJointsRowChain = array();
$schematicLEDsColumnChain = array();
$schematicLEDsRowChain = array();
$schematicPinsColumnChain = array();
$schematicPinsRowChain = array();
$schematicTerminalsColumnChain = array();
$schematicTerminalsRowChain = array();
$schematicTextsColumnChain = array();
$schematicTextsRowChain = array();
$schematicI = 0;
foreach ($globalPins["C"] as $key => $value) {
	array_push($schematicColumnLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#column-line-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicJointsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0) rotate(-45)%s", $schematicI * 30, (($globalDirection) ? "" : " scale(1,-1)")),
		),
	));
	array_push($schematicPinsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0) rotate(90)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicTextsColumnChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
		"tags" => $key + 1,
	));
	$schematicI++;
}
$schematicI = 0;
foreach ($globalPins["R"] as $key => $value) {
	array_push($schematicRowLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#row-line-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicJointsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicPinsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTextsRowChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
		"tags" => $key + 1,
	));
	$schematicI++;
}
$schematicI = 0;
foreach ($globalPins["E"] as $key => $value) {
	array_push($schematicPinsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTextsRowChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
			"xml:space" => "preserve",
		),
		"tags" => (($value == "NC") ? "" : " ," . ($key + 1)),
	));
	$schematicI++;
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-al-template",
						"points" => "0,0 4,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-ar-template",
						"points" => "4,0 8,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-b-template",
						"points" => "8,0 8,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-c-template",
						"points" => "8,4 8,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-dr-template",
						"points" => "8,8 4,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-dl-template",
						"points" => "4,8 0,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-e-template",
						"points" => "0,8 0,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-f-template",
						"points" => "0,4 0,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-gl-template",
						"points" => "0,4 4,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-gr-template",
						"points" => "4,4 8,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-up-template",
						"points" => "4,4 4,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-dn-template",
						"points" => "4,4 4,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-upr-template",
						"points" => "4,4 8,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-dnl-template",
						"points" => "4,4 0,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-upl-template",
						"points" => "4,4 0,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-dnr-template",
						"points" => "4,4 8,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "segment-p-template",
						"cx" => 10,
						"cy" => 8,
						"r" => $schematicBorderThickness / 2,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-template",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicBorderThickness,
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-al-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-ar-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dr-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dl-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gl-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gr-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-up-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dn-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upr-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnl-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upl-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnr-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-template",
								"fill" => $schematicPinColor,
								"stroke" => "none",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-al-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-al-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-ar-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-ar-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-b-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-c-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-dr-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dr-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-dl-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dl-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-e-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-f-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-gl-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gl-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-gr-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gr-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-up-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-up-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-dn-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dn-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-upr-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upr-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-dnl-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnl-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-upl-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upl-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-dnr-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnr-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-p-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-template",
								"fill" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "row-line-template",
						"points" => sprintf("0,0 %s,0", (count($globalPins["C"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "column-line-template",
						"points" => sprintf("0,0 0,%s", (count($globalPins["R"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "joint-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 2,
						"fill" => $schematicBorderColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "joint-row-template",
					),
					"tags" => $schematicJointsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "direction-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-1,0 1,0 0,2",
								"fill" => $schematicBorderColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-2 0,0",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness / 2,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-4,-5 4,-5 0,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-10 0,10",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "-4,5 4,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(-3,8)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,0) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,2) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-row-template",
					),
					"tags" => $schematicLEDsColumnChain,
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicColumnLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicRowLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(27.5,7.5)",
					),
					"tags" => $schematicLEDsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight - 10),
					),
					"tags" => $schematicPinsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(10,0)",
					),
					"tags" => $schematicPinsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight),
					),
					"tags" => $schematicTerminalsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(0,0)",
					),
					"tags" => $schematicTerminalsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(16,%s)", $schematicHeight - 20),
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-al-highlight",
								"transform" => "translate(0,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-ar-highlight",
								"transform" => "translate(30,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-highlight",
								"transform" => "translate(60,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-highlight",
								"transform" => "translate(90,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dr-highlight",
								"transform" => "translate(120,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dl-highlight",
								"transform" => "translate(150,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-highlight",
								"transform" => "translate(180,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-highlight",
								"transform" => "translate(210,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gl-highlight",
								"transform" => "translate(240,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-gr-highlight",
								"transform" => "translate(270,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-up-highlight",
								"transform" => "translate(300,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dn-highlight",
								"transform" => "translate(330,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upr-highlight",
								"transform" => "translate(360,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnl-highlight",
								"transform" => "translate(390,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-upl-highlight",
								"transform" => "translate(420,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-dnr-highlight",
								"transform" => "translate(450,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-highlight",
								"transform" => "translate(480,0)",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $schematicTextSize,
						"font-family" => $globalFontFamily,
						"fill" => $schematicTextColor,
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "end",
								"transform" => sprintf("translate(33,%s)", $schematicHeight - 2),
							),
							"tags" => $schematicTextsColumnChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "start",
								"transform" => "translate(2,10)",
							),
							"tags" => $schematicTextsRowChain,
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}