<?php

// global settings
$globalPins = array(
	"VIN+" => "Input Voltage+",
	"VIN-" => "Input Voltage-",
	"BAT+" => "Battery Charge+",
	"BAT-" => "Battery Charge-",
);
$globalTitle = "FC75";
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = $globalTitle;
$fritzingProperties = array(
	"family" => "A Battery Charger",
	"model" => $fritzingLabel,
	"package" => "THT",
	"pins" => count($globalPins),
);
$fritzingTags = array($globalTitle, $fritzingProperties["package"]);

// icon settings
$iconUnit = "in";
$iconBackgroundColor = "#000088";
$iconConnectorColor = "#888888";
$iconPinHole = "#000000";
$iconPinColor = "#FFBF00";
$iconPinThickness = 1.5;
$iconTextColor = "#FFFFFF";
$iconPinRadius = 3;
$iconTextSize = 5;
$iconTitleSize = 10;

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardBackgroundColor = $iconBackgroundColor;
$breadboardConnectorColor = $iconConnectorColor;
$breadboardPinHole = $iconPinHole;
$breadboardPinColor = $iconPinColor;
$breadboardPinThickness = $iconPinThickness;
$breadboardTextColor = $iconTextColor;
$breadboardPinRadius = $iconPinRadius;
$breadboardTextSize = $iconTextSize;
$breadboardTitleSize = $iconTitleSize;

// pcb settings
$pcbUnit = $breadboardUnit;
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbBackgroundColor = "none";
$pcbPinColor = $breadboardPinColor;
$pcbPinThickness = $breadboardPinThickness;
$pcbPinRadius = $breadboardPinRadius;
$pcbTextSize = $breadboardTextSize;
$pcbTitleSize = $breadboardTitleSize;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 5;
$schematicTitleSize = 5;
$schematicTextColor = $schematicPinColor;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 2.5;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 60;
$iconHeight = 60;
$breadboardWidth = $iconWidth;
$breadboardHeight = 70;
$pcbWidth = $breadboardWidth;
$pcbHeight = $breadboardHeight;
$schematicWidth = 80;
$schematicHeight = 30;

$now = getdate();
$multiple = 100;

$filenameBasename = "fc75-battery-charger";
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
foreach ($globalPins as $key => $value) {
	$id = sprintf("connector-%s", $key);
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => $key,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => $value,
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $iconPinRadius,
						"fill" => $iconPinHole,
						"stroke" => $iconPinColor,
						"stroke-width" => $iconPinThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "1st-pin-template",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -$iconPinRadius,
								"y" => -$iconPinRadius,
								"width" => $iconPinRadius * 2,
								"height" => $iconPinRadius * 2,
								"fill" => $iconPinHole,
								"stroke" => $iconPinColor,
								"stroke-width" => $iconPinThickness,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-template",
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"fill" => $iconBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 20,
								"y" => 0,
								"width" => 20,
								"height" => 15,
								"fill" => $iconConnectorColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,0)", $iconWidth / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,5)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#1st-pin-template",
										"transform" => sprintf("translate(%s,0)", $iconWidth / 2 - 5),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#pin-template",
										"transform" => sprintf("translate(%s,0)", $iconWidth / -2 + 5),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,65)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#1st-pin-template",
										"transform" => sprintf("translate(%s,0)", $iconWidth / 2 - 5),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#pin-template",
										"transform" => sprintf("translate(%s,0)", $iconWidth / -2 + 5),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "rotate(90)",
								"font-family" => $globalFontFamily,
								"fill" => $iconTextColor,
								"stroke" => "none",
							),
							"tags" => array(
								array(
									"name" => "text",
									"attributes" => array(
										"x" => $iconHeight / 2,
										"y" => fontBaselineOffset($iconTitleSize),
										"font-size" => $iconTitleSize,
										"text-anchor" => "middle",
									),
									"tags" => "FC75",
								),
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $iconTextSize,
										"transform" => sprintf("translate(0,%s)", fontBaselineOffset($iconTextSize)),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "start",
												"transform" => "translate(10,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "VIN+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "VIN-",
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "end",
												"transform" => "translate(60,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "BAT+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "BAT-",
												),
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $breadboardPinRadius,
						"fill" => $breadboardPinHole,
						"stroke" => $breadboardPinColor,
						"stroke-width" => $breadboardPinThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "1st-pin-template",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -$breadboardPinRadius,
								"y" => -$breadboardPinRadius,
								"width" => $breadboardPinRadius * 2,
								"height" => $breadboardPinRadius * 2,
								"fill" => $breadboardPinHole,
								"stroke" => $breadboardPinColor,
								"stroke-width" => $breadboardPinThickness,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#pin-template",
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardBackgroundColor,
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 20,
								"y" => 0,
								"width" => 20,
								"height" => 15,
								"fill" => $breadboardConnectorColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,5)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN+-pin",
										"xlink:href" => "#1st-pin-template",
										"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2 - 5),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN--pin",
										"xlink:href" => "#pin-template",
										"transform" => sprintf("translate(%s,0)", $breadboardWidth / -2 + 5),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,65)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT+-pin",
										"xlink:href" => "#1st-pin-template",
										"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2 - 5),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT--pin",
										"xlink:href" => "#pin-template",
										"transform" => sprintf("translate(%s,0)", $breadboardWidth / -2 + 5),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "rotate(90)",
								"font-family" => $globalFontFamily,
								"fill" => $breadboardTextColor,
								"stroke" => "none",
							),
							"tags" => array(
								array(
									"name" => "text",
									"attributes" => array(
										"x" => $breadboardHeight / 2,
										"y" => fontBaselineOffset($breadboardTitleSize),
										"font-size" => $breadboardTitleSize,
										"text-anchor" => "middle",
									),
									"tags" => "FC75",
								),
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $breadboardTextSize,
										"transform" => sprintf("translate(0,%s)", fontBaselineOffset($breadboardTextSize)),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "start",
												"transform" => "translate(10,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "VIN+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "VIN-",
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "end",
												"transform" => "translate(60,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "BAT+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "BAT-",
												),
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2),
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "silkscreen",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => $pcbWidth / -2,
								"y" => 0,
								"width" => $pcbWidth,
								"height" => $pcbHeight,
								"fill" => $pcbBackgroundColor,
								"stroke" => $pcbSilkscreenColor,
								"stroke-width" => $pcbSilkscreenThickness
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "rotate(90)",
								"font-family" => $globalFontFamily,
								"fill" => $pcbSilkscreenColor,
								"stroke" => $pcbBackgroundColor,
							),
							"tags" => array(
								array(
									"name" => "text",
									"attributes" => array(
										"x" => $pcbHeight / 2,
										"y" => fontBaselineOffset($pcbTitleSize),
										"font-size" => $pcbTitleSize,
										"text-anchor" => "middle",
									),
									"tags" => "FC75",
								),
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $pcbTextSize,
										"transform" => sprintf("translate(0,%s)", fontBaselineOffset($pcbTextSize)),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "start",
												"transform" => "translate(10,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "VIN+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "VIN-",
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"text-anchor" => "end",
												"transform" => "translate(60,0)",
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => -25,
													),
													"tags" => "BAT+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 0,
														"y" => 25,
													),
													"tags" => "BAT-",
												),
											),
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper1",
						"class" => "top-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"id" => "copper0",
								"class" => "bottom-layer",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"opacity" => 0,
										"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
									),
									"tags" => array(
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinThickness,
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"id" => "1st-pin-template",
											),
											"tags" => array(
												array(
													"name" => "rect",
													"attributes" => array(
														"x" => -$pcbPinRadius,
														"y" => -$pcbPinRadius,
														"width" => $pcbPinRadius * 2,
														"height" => $pcbPinRadius * 2,
														"fill" => $pcbBackgroundColor,
														"stroke" => $pcbPinColor,
														"stroke-width" => $pcbPinThickness,
													),
												),
												array(
													"name" => "circle",
													"attributes" => array(
														"cx" => 0,
														"cy" => 0,
														"r" => $pcbPinRadius,
														"fill" => $pcbBackgroundColor,
														"stroke" => $pcbPinColor,
														"stroke-width" => $pcbPinThickness,
													),
												),
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,5)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VIN+-pin",
												"xlink:href" => "#1st-pin-template",
												"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2 - 5),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VIN--pin",
												"xlink:href" => "#pin-template",
												"transform" => sprintf("translate(%s,0)", $breadboardWidth / -2 + 5),
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,65)",
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-BAT+-pin",
												"xlink:href" => "#1st-pin-template",
												"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2 - 5),
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-BAT--pin",
												"xlink:href" => "#pin-template",
												"transform" => sprintf("translate(%s,0)", $breadboardWidth / -2 + 5),
											),
										),
									),
								),
							),
						),
					),
				),
			)
		),
	),
);

// schematic
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 20,
						"y" => 0,
						"width" => 40,
						"height" => 30,
						"fill" => $schematicBackgroundColor,
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,0)", $schematicWidth / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"font-family" => $globalFontFamily,
								"text-anchor" => "middle",
								"fill" => $schematicTextColor,
								"stroke" => "none",
							),
							"tags" => array(
								array(
									"name" => "text",
									"attributes" => array(
										"x" => 0,
										"y" => 15 + fontBaselineOffset($schematicTitleSize),
										"font-size" => $schematicTitleSize,
									),
									"tags" => $globalTitle,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $schematicTextSize,
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", 10 - fontBaselineOffset($schematicTextSize)),
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 30,
													),
													"tags" => "VIN+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => -30,
													),
													"tags" => "VIN-",
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", 20 - fontBaselineOffset($schematicTextSize)),
											),
											"tags" => array(
												array(
													"name" => "text",
													"attributes" => array(
														"x" => 30,
													),
													"tags" => "BAT+",
												),
												array(
													"name" => "text",
													"attributes" => array(
														"x" => -30,
													),
													"tags" => "BAT-",
												),
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,10)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN+-pin",
										"xlink:href" => "#pin-template",
										"transform" => "translate(30,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN+-terminal",
										"xlink:href" => "#terminal-template",
										"transform" => "translate(40,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN--pin",
										"xlink:href" => "#pin-template",
										"transform" => "translate(-30,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-VIN--terminal",
										"xlink:href" => "#terminal-template",
										"transform" => "translate(-40,0)",
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,20)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT+-pin",
										"xlink:href" => "#pin-template",
										"transform" => "translate(30,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT+-terminal",
										"xlink:href" => "#terminal-template",
										"transform" => "translate(40,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT--pin",
										"xlink:href" => "#pin-template",
										"transform" => "translate(-30,0)",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"id" => "connector-BAT--terminal",
										"xlink:href" => "#terminal-template",
										"transform" => "translate(-40,0)",
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}