<?php

$globalPins = array(
	"GND1" => array("description" => "Ground 1 (Black)", "layer" => array("top")),
	"TX+" => array("description" => "Superspeed Transmitter+ (Orange)", "layer" => array("top")),
	"TX-" => array("description" => "Superspeed Transmitter- (Purple)", "layer" => array("top")),
	"VCC1" => array("description" => "Positive Power Supply 1 (Red)", "layer" => array("top")),
	"CC" => array("description" => "Configuration Channel", "layer" => array("top")),
	"D+" => array("description" => "Data+ (2.0) (White)", "layer" => array("top")),
	"D-" => array("description" => "Data- (2.0) (Green)", "layer" => array("top")),
	"SBU" => array("description" => "SideBand Use", "layer" => array("top")),
	"VCC2" => array("description" => "Positive Power Supply 2 (Red)", "layer" => array("top")),
	"RX-" => array("description" => "Superspeed Receiver- (Blue)", "layer" => array("top")),
	"RX+" => array("description" => "Superspeed Receiver+ (Yellow)", "layer" => array("top")),
	"GND2" => array("description" => "Ground 2 (Black)", "layer" => array("top")),
	"SBL.GND" => array("description" => "Shield Back Left Ground", "layer" => array("top", "bottom")),
	"SBR.GND" => array("description" => "Shield Back Right Ground", "layer" => array("top", "bottom")),
	"SFL.GND" => array("description" => "Shield Front Left Ground", "layer" => array("top", "bottom")),
	"SFR.GND" => array("description" => "Shield Front Right Ground", "layer" => array("top", "bottom")),
);
$globalTitle = "USB 3.0 Type-C";
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");
define("SVG_XML_UUID", "04998e2d-bec5-4247-9640-a9fc278d29e2");
// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "USB";
$fritzingProperties = array(
	"family" => sprintf("A %s", $fritzingLabel),
	"package" => "SMT",
	"pins" => count($globalPins),
	"size" => "Standard",
	"type" => "C",
	"version" => "3.0",
);
$fritzingTags = array($fritzingLabel, $fritzingProperties["package"]);

// icon settings
$iconUnit = "mm";
$iconPinWidth = 0.3;
$iconPinHeight = 0.25;

// breadboard settings
$breadboardUnit = "in";
$breadboardMagnify = 5;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 5;
$pcbPinHeight = 15;
$pcbShieldRadius = 15;
$pcbShieldThickness = 2;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTextSize = 5;
$pcbTitleSize = 10;

// schematic settings
$schematicUnit = $breadboardUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 3;

// Don't change the code below, unless you know what to do...
function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 8.25;
$iconHeight = 2.42;
$breadboardWidth = 80;
$breadboardHeight = 40;
$pcbMultiple = 10;
$pcbWidth = 110;
$pcbHeight = 75;
$schematicWidth = 110;
$schematicHeight = 70;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("usb-3.0-type-c");
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
foreach ($globalPins as $key => $value) {
	$id = sprintf("connector-%s", $key);
	$pcbViews = array();
	if (in_array("top", $value["layer"])) {
		array_push($pcbViews, array(
			"name" => "p",
			"attributes" => array(
				"svgId" => sprintf("%s-pin", $id),
				"terminalId" => sprintf("%s-terminal", $id),
//				"legId" => sprintf("%s-leg", $id),
				"layer" => "copper1",
			),
		));
	}
	if (in_array("bottom", $value["layer"])) {
		array_push($pcbViews, array(
			"name" => "p",
			"attributes" => array(
				"svgId" => sprintf("%s-pin", $id),
				"terminalId" => sprintf("%s-terminal", $id),
//				"legId" => sprintf("%s-leg", $id),
				"layer" => "copper0",
			),
		));
	}
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => $key,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => $value["description"],
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => $pcbViews,
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => ((defined("SVG_XML_UUID")) ? SVG_XML_UUID : uuidgen()),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$iconPinsChain = array();
for ($iconI = 0; $iconI < 12; $iconI++) {
	$iconX = $iconI * 0.5 - 2.75;
	array_push($iconPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $iconX),
		),
	));
}
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "pin-template",
						"x" => $iconPinWidth / -2,
						"y" => $iconPinHeight / -2,
						"width" => $iconPinWidth,
						"height" => $iconPinHeight,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "pin-row-template",
					),
					"tags" => $iconPinsChain,
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "#000000",
						"stroke-width" => 0.1,
					),
					"tags" => array(
						array(
							"name" => "path",
							"attributes" => array(
								"d" => sprintf(
									'M %2$s,%1$s A %2$s,%2$s 0 0 0 %1$s,%2$s %2$s,%2$s 0 0 0 %2$s,%3$s h %4$s A %2$s,%2$s 0 0 0 %6$s,%2$s %2$s,%2$s 0 0 0 %5$s,%1$s Z',
									0,
									$iconHeight / 2,
									$iconHeight,
									$iconWidth - $iconHeight,
									$iconWidth - $iconHeight / 2,
									$iconWidth
								),
								"fill" => "#DDDDDD",
							),
						),
						array(
							"name" => "path",
							"attributes" => array(
								"d" => sprintf(
									'M %2$s,%1$s A %1$s,%1$s 0 0 0 %1$s,%2$s %1$s,%1$s 0 0 0 %2$s,%3$s h %6$s A %1$s,%1$s 0 0 0 %4$s,%2$s %1$s,%1$s 0 0 0 %5$s,%1$s Z',
									$iconHeight / 4,
									$iconHeight / 2,
									$iconHeight / 4 * 3,
									$iconWidth - $iconHeight / 4,
									$iconWidth - $iconHeight / 2,
									$iconWidth - $iconHeight
								),
								"fill" => "#BBBBBB",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => "#FFFF00",
								"transform" => sprintf("translate(%s,%s)", $iconWidth / 2, $iconHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $iconHeight / 4),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"fill" => "#FFFF00",
										"transform" => sprintf("translate(0,%s)", $iconHeight / -4),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardPinsChain = array();
for ($breadboardI = 0; $breadboardI < 12; $breadboardI++) {
	$breadboardX = ($breadboardI * 0.5 - 2.75) * $breadboardMagnify;
	array_push($breadboardPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#icon-pin-template",
			"transform" => sprintf("translate(%s,0)", $breadboardX),
		),
	));
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "direction-template",
						"points" => "-1.5,-1.5 1.5,-1.5 0.5,-0.5 -0.5,-0.5",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "pin-template",
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => -1.5,
								"y" => -1.5,
								"width" => 3,
								"height" => 3,
								"fill" => "#AAAA60",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#direction-template",
								"fill" => "#888860",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(90)",
								"xlink:href" => "#direction-template",
								"fill" => "#777760",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(180)",
								"xlink:href" => "#direction-template",
								"fill" => "#999960",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "rotate(-90)",
								"xlink:href" => "#direction-template",
								"fill" => "#BBBB60",
							),
						),
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "icon-pin-template",
						"x" => ($iconPinWidth / -2) * $breadboardMagnify,
						"y" => ($iconPinHeight / -2) * $breadboardMagnify,
						"width" => $iconPinWidth * $breadboardMagnify,
						"height" => $iconPinHeight * $breadboardMagnify,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "icon-pin-row-template",
					),
					"tags" => $breadboardPinsChain,
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => "#004400",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $globalInterval / 2, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / 2),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-pin",
												"transform" => "translate(0,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND1-pin",
												"transform" => "translate(10,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-pin",
												"transform" => "translate(20,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--pin",
												"transform" => "translate(30,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC1-pin",
												"transform" => "translate(40,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-CC-pin",
												"transform" => "translate(50,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-pin",
												"transform" => "translate(60,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-pin",
												"transform" => "translate(70,0)",
												"xlink:href" => "#pin-template",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / -2),
									),
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-pin",
												"transform" => "translate(0,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND2-pin",
												"transform" => "translate(10,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-pin",
												"transform" => "translate(20,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--pin",
												"transform" => "translate(30,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC2-pin",
												"transform" => "translate(40,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBU-pin",
												"transform" => "translate(50,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--pin",
												"transform" => "translate(60,0)",
												"xlink:href" => "#pin-template",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-pin",
												"transform" => "translate(70,0)",
												"xlink:href" => "#pin-template",
											),
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "#000000",
						"stroke-width" => 0.1 * $breadboardMagnify,
						"transform" => sprintf("translate(%s,%s)", ($breadboardWidth - $iconWidth * $breadboardMagnify) / 2, ($breadboardHeight - $iconHeight * $breadboardMagnify) / 2),
					),
					"tags" => array(
						array(
							"name" => "path",
							"attributes" => array(
								"d" => sprintf(
									'M %2$s,%1$s A %2$s,%2$s 0 0 0 %1$s,%2$s %2$s,%2$s 0 0 0 %2$s,%3$s h %4$s A %2$s,%2$s 0 0 0 %6$s,%2$s %2$s,%2$s 0 0 0 %5$s,%1$s Z',
									0,
									($iconHeight / 2) * $breadboardMagnify,
									$iconHeight * $breadboardMagnify,
									($iconWidth - $iconHeight) * $breadboardMagnify,
									($iconWidth - $iconHeight / 2) * $breadboardMagnify,
									$iconWidth * $breadboardMagnify
								),
								"fill" => "#DDDDDD",
							),
						),
						array(
							"name" => "path",
							"attributes" => array(
								"d" => sprintf(
									'M %2$s,%1$s A %1$s,%1$s 0 0 0 %1$s,%2$s %1$s,%1$s 0 0 0 %2$s,%3$s h %6$s A %1$s,%1$s 0 0 0 %4$s,%2$s %1$s,%1$s 0 0 0 %5$s,%1$s Z',
									$iconHeight / 4 * $breadboardMagnify,
									$iconHeight / 2 * $breadboardMagnify,
									$iconHeight / 4 * 3 * $breadboardMagnify,
									($iconWidth - $iconHeight / 4) * $breadboardMagnify,
									($iconWidth - $iconHeight / 2) * $breadboardMagnify,
									($iconWidth - $iconHeight) * $breadboardMagnify
								),
								"fill" => "#BBBBBB",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => "#FFFF00",
								"transform" => sprintf("translate(%s,%s)", $iconWidth / 2 * $breadboardMagnify, $iconHeight / 2 * $breadboardMagnify),
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#icon-pin-row-template",
										"transform" => sprintf("translate(0,%s)", $iconHeight / 4 * $breadboardMagnify),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"xlink:href" => "#icon-pin-row-template",
										"transform" => sprintf("translate(0,%s)", $iconHeight / -4 * $breadboardMagnify),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $pcbMultiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $pcbMultiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"transform" => sprintf("translate(%s,0)", $pcbWidth / 2),
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "silkscreen",
					),
					"tags" => array(
						array(
							"name" => "circle",
							"attributes" => array(
								"cx" => -37.25,
								"cy" => 5,
								"r" => $pcbSilkscreenThickness,
								"fill" => $pcbSilkscreenColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"font-family" => $globalFontFamily,
								"fill" => $pcbSilkscreenColor,
								"stroke" => "none",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $pcbTextSize,
										"text-anchor" => "start",
										"transform" => sprintf("translate(%s,22.5)", $pcbTextSize * -0.4),
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 31.25,
											),
											"tags" => "GND1",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 23.75,
											),
											"tags" => "TX+",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 17.5,
											),
											"tags" => "TX-",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 12.5,
											),
											"tags" => "VCC1",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 7.5,
											),
											"tags" => "CC",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => 2.5,
											),
											"tags" => "D+",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -2.5,
											),
											"tags" => "D-",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -7.5,
											),
											"tags" => "SBU",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -12.5,
											),
											"tags" => "VCC2",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -17.5,
											),
											"tags" => "RX-",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -23.75,
											),
											"tags" => "RX+",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"transform" => "rotate(90)",
												"x" => 0,
												"y" => -31.25,
											),
											"tags" => "GND2",
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"font-size" => $pcbTitleSize,
										"text-anchor" => "middle",
										"transform" => sprintf("translate(0,%s)", $pcbHeight / 2 + $pcbTitleSize * 2),
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 0,
											),
											"tags" => "USB (3.0)",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => $pcbTitleSize,
											),
											"tags" => "Type-C",
										),
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper1",
						"class" => "top-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"id" => "copper0",
								"class" => "bottom-layer",
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"opacity" => 0,
										"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"id" => "thick-pin-template",
												"x" => $pcbPinWidth / -2,
												"y" => 0,
												"width" => $pcbPinWidth,
												"height" => $pcbPinHeight,
												"fill" => $pcbPinColor,
												"stroke" => $pcbBackgroundColor,
											),
										),
										array(
											"name" => "rect",
											"attributes" => array(
												"id" => "thin-pin-template",
												"x" => $pcbPinWidth / -4,
												"y" => 0,
												"width" => $pcbPinWidth / 2,
												"height" => $pcbPinHeight,
												"fill" => $pcbPinColor,
												"stroke" => $pcbBackgroundColor,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "terminal-template",
												"cx" => 0,
												"cy" => 0,
												"r" => 0.001,
											),
										),
										array(
											"name" => "path",
											"attributes" => array(
												"id" => "shield-template",
												"d" => "m 0,-10 c -2.761424,0 -5,2.2387878 -5,5 v 10 c 0,2.7613873 2.238308,5 5,5 2.761589,0 5,-2.238623 5,-5 v -10 c 0,-2.7614238 -2.238788,-5 -5,-5 z",
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbShieldThickness,
											),
										),
									),
								),
								array(
									"name" => "g",
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,17.5)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SBL.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(-45,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SBR.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(45,0)",
													),
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,62.5)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SFL.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(-45,0)",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"id" => "connector-SFR.GND-pin",
														"xlink:href" => "#shield-template",
														"transform" => "translate(45,0)",
													),
												),
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(0,5)",
							),
							"tags" => array(
								array(
									"name" => "g",
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND1-pin",
												"xlink:href" => "#thick-pin-template",
												"transform" => "translate(-31.25,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-pin",
												"xlink:href" => "#thick-pin-template",
												"transform" => "translate(-23.75,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(-17.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC1-pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(-12.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-CC-pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(-7.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(-2.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(2.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBU-pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(7.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC2-pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(12.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--pin",
												"xlink:href" => "#thin-pin-template",
												"transform" => "translate(17.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-pin",
												"xlink:href" => "#thick-pin-template",
												"transform" => "translate(23.75,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND2-pin",
												"xlink:href" => "#thick-pin-template",
												"transform" => "translate(31.25,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"tags" => array(
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND1-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-31.25,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-23.75,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-17.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC1-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-12.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-CC-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-7.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-2.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(2.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBU-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(7.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC2-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(12.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(17.5,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(23.75,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND2-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(31.25,0)",
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $schematicTextSize,
						"font-family" => $globalFontFamily,
						"text-anchor" => "middle",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 20,
								"y" => 20,
								"width" => $schematicWidth - 40,
								"height" => $schematicHeight - 40,
								"fill" => $schematicBackgroundColor,
								"stroke" => $schematicBorderColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $schematicWidth / 2),
								"fill" => $schematicPinColor,
								"stroke" => $schematicPinColor,
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,30)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => -45,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SBL.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-45,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBL.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-55,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 45,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SBR.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(45,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBR.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(55,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,40)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => -45,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SFL.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-45,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFL.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-55,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 45,
												"y" => -1,
												"stroke" => "none",
											),
											"tags" => "SFR.GND",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(45,0)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SFR.GND-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(55,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"stroke" => "none",
										"transform" => "translate(0,35)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => 0,
											),
											"tags" => "USB (3.0)",
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => $schematicTextSize,
											),
											"tags" => "Type-C",
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $schematicHeight),
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-28.75,-10) rotate(90)",
											),
											"tags" => "GND1",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND1-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-25,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND1-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-25,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-18.75,-10) rotate(90)",
											),
											"tags" => "TX+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-15,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-15,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-8.75,-10) rotate(90)",
											),
											"tags" => "TX-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-5,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-TX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-5,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(6,-10) rotate(90)",
											),
											"tags" => "VCC1",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC1-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(5,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC1-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(5,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(16,-10) rotate(90)",
											),
											"tags" => "CC",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-CC-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(15,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-CC-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(15,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(26,-10) rotate(90)",
											),
											"tags" => "D+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(25,-10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(25,0)",
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,0)",
									),
									"tags" => array(
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(26,10) rotate(90)",
											),
											"tags" => "D-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(25,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-D--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(25,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(16,10) rotate(90)",
											),
											"tags" => "SBU",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBU-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(15,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-SBU-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(15,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(6,10) rotate(90)",
											),
											"tags" => "VCC2",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC2-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(5,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-VCC2-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(5,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-8.75,10) rotate(90)",
											),
											"tags" => "RX-",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-5,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX--terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-5,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-18.75,10) rotate(90)",
											),
											"tags" => "RX+",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-15,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-RX+-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-15,0)",
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"stroke" => "none",
												"transform" => "translate(-28.75,10) rotate(90)",
											),
											"tags" => "GND2",
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND2-pin",
												"xlink:href" => "#pin-template",
												"transform" => "translate(-25,10) rotate(90)",
											),
										),
										array(
											"name" => "use",
											"attributes" => array(
												"id" => "connector-GND2-terminal",
												"xlink:href" => "#terminal-template",
												"transform" => "translate(-25,0)",
											),
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}