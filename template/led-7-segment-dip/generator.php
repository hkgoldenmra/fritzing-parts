<?php

// global settings
$globalColor = "Red";
$globalPins = array(
	"R" => array(2 => "1A"),
	"C" => array(6 => "A", 5 => "B", 3 => "C", 1 => "D", 0 => "E", 8 => "F", 9 => "G", 4 => "P"),
	"E" => array(7 => "1B"),
//	"R" => array(6 => "1", 7 => "2"),
//	"C" => array(9 => "A", 8 => "B", 0 => "C", 3 => "D", 2 => "E", 5 => "F", 4 => "G", 1 => "P"),
//	"E" => array(),
//	"R" => array(7 => "1", 8 => "2", 11 => "3"),
//	"C" => array(10 => "A", 6 => "B", 3 => "C", 1 => "D", 0 => "E", 9 => "F", 4 => "G", 2 => "P"),
//	"E" => array(5 => "NC"),
//	"R" => array(5 => "1", 7 => "2", 8 => "3", 11 => "4"),
//	"C" => array(10 => "A", 6 => "B", 3 => "C", 1 => "D", 0 => "E", 9 => "F", 4 => "G", 2 => "P"),
//	"E" => array(),
//	"R" => array(5 => "1", 7 => "2", 10 => "3", 11 => "4", 13 => "5"),
//	"C" => array(8 => "A", 12 => "B", 3 => "C", 1 => "D", 0 => "E", 9 => "F", 4 => "G", 2 => "P"),
//	"E" => array(6 => "NC"),
//	"R" => array(5 => "1", 6 => "2", 7 => "3", 10 => "4", 11 => "5", 13 => "6"),
//	"C" => array(8 => "A", 12 => "B", 3 => "C", 1 => "D", 0 => "E", 9 => "F", 4 => "G", 2 => "P"),
//	"E" => array(),
);
$globalDirection = true;
$globalTitle = sprintf("LED 7 Segment DIP %s Digit %s %s", count($globalPins["R"]), $globalColor, (($globalDirection) ? "Anode" : "Cathode"));
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "7SEG";
$fritzingTags = array("LED", "7SEG", "DIP");
$fritzingProperties = array(
	"color" => $globalColor,
	"digits" => count($globalPins["R"]),
	"common" => (($globalDirection) ? "Anode" : "Cathode"),
	"family" => "A LED 7 Segment",
	"package" => "DIP",
);

// icon settings
$iconUnit = "in";
$iconShieldColor = "#000000";
$iconLEDColor = "#888888";

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardShieldColor = $iconShieldColor;
$breadboardLEDColor = $iconLEDColor;
$breadboardPinRadius = 2.5;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTextSize = 5;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 8;
$schematicTextColor = $schematicPinColor;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 4;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 70;
$iconHeight = 70;
$breadboardWidth = count($globalPins["R"]) * 50;
$breadboardHeight = $iconHeight;
$pcbWidth = $breadboardWidth;
$pcbHeight = $breadboardHeight;
$schematicWidth = count($globalPins["C"]) * 30 + 5;
$schematicHeight = count($globalPins["R"]) * 30 + 5;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("led-7-segment-dip-%s-digit-%s-%s", count($globalPins["R"]), $globalColor, (($globalDirection) ? "Anode" : "Cathode"));
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
$fritzingPins = array();
foreach ($globalPins as $key => $array) {
	foreach ($array as $index => $value) {
		$fritzingPins[$index] = array("name" => $value, "direction" => (($key == "R" || $key == "E") && $globalDirection) || ($key == "C" && !$globalDirection));
	}
}
ksort($fritzingPins);
foreach ($fritzingPins as $value) {
	$id = sprintf("connector-%s", $value["name"]);
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "7-segment-template",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $globalColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m43.264 11.735 1.4896 2.9821-2.9779 16.392-2.9834 2.9765-2.9765-2.9765 2.9765-14.903z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m38.792 35.576 1.4924 2.9779-2.9793 16.392-2.9807 2.9793-2.9793-2.9793 2.9793-14.899z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m41.776 10.248-2.9838 4.4688h-19.368l-2.9765-4.469 1.4879-1.4884h22.349z",
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $iconLEDColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m7.5054 59.41 2.9807-4.4648h19.371l2.9779 4.469-1.4897 1.4883h-22.35z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m7.5054 38.554 2.9807-2.9779 2.9779 2.9779-2.9779 14.899-4.4731 4.4717-1.4868-2.9793z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m34.325 31.108 2.9807 2.9765-2.9807 2.9793h-19.37l-2.9793-2.9793 2.9793-2.9765z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m17.936 14.717-2.9807 14.902-4.469 4.4662-1.491-2.9765 2.9807-16.392 2.9793-2.9821z",
									),
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => 41.137,
										"cy" => 57.921,
										"r" => 3.1382,
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"transform" => "translate(10,0)",
								"xlink:href" => "#7-segment-template",
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardTemplatesChain = array();
$breadboardFrontPinsChain = array();
$breadboardBackPinsChain = array();
for ($breadboardI = 0; $breadboardI < count($globalPins["R"]); $breadboardI++) {
	array_push($breadboardTemplatesChain, array(
		"name" => "use",
		"attributes" => array(
			"transform" => sprintf("translate(%s,0)", $breadboardI * $breadboardWidth / count($globalPins["R"])),
			"xlink:href" => "#7-segment-template",
		),
	));
}
$breadboardI = 0;
foreach ($fritzingPins as $value) {
	if ($breadboardI < count($fritzingPins) / 2) {
		array_push($breadboardFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", $breadboardI * $globalInterval),
				"xlink:href" => (($breadboardI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		array_push($breadboardBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", (count($fritzingPins) - $breadboardI - 1) * $globalInterval),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$breadboardI++;
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $breadboardPinRadius,
						"opacity" => 0,
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "1st-pin-template",
						"x" => -$breadboardPinRadius,
						"y" => -$breadboardPinRadius,
						"width" => $breadboardPinRadius * 2,
						"height" => $breadboardPinRadius * 2,
						"opacity" => 0,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "7-segment-template",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $globalColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m43.264 11.735 1.4896 2.9821-2.9779 16.392-2.9834 2.9765-2.9765-2.9765 2.9765-14.903z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m38.792 35.576 1.4924 2.9779-2.9793 16.392-2.9807 2.9793-2.9793-2.9793 2.9793-14.899z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m41.776 10.248-2.9838 4.4688h-19.368l-2.9765-4.469 1.4879-1.4884h22.349z",
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"fill" => $iconLEDColor,
							),
							"tags" => array(
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m7.5054 59.41 2.9807-4.4648h19.371l2.9779 4.469-1.4897 1.4883h-22.35z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m7.5054 38.554 2.9807-2.9779 2.9779 2.9779-2.9779 14.899-4.4731 4.4717-1.4868-2.9793z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m34.325 31.108 2.9807 2.9765-2.9807 2.9793h-19.37l-2.9793-2.9793 2.9793-2.9765z",
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"d" => "m17.936 14.717-2.9807 14.902-4.469 4.4662-1.491-2.9765 2.9807-16.392 2.9793-2.9821z",
									),
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => 41.137,
										"cy" => 57.921,
										"r" => 3.1382,
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardShieldColor,
							),
						),
						array(
							"name" => "g",
							"tags" => $breadboardTemplatesChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", ($breadboardWidth - (count($fritzingPins) / 2 - 1) * $globalInterval) / 2, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / 2),
									),
									"tags" => $breadboardFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($breadboardHeight - $globalInterval) / -2),
									),
									"tags" => $breadboardBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontTextsChain = array();
$pcbBackTextsChain = array();
$pcbFrontPinsChain = array();
$pcbBackPinsChain = array();
$pcbI = 0;
foreach ($fritzingPins as $value) {
	if ($pcbI < count($fritzingPins) / 2) {
		$pcbX = $pcbI * $globalInterval;
		array_push($pcbFrontTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
			"tags" => $value["name"],
		));
		array_push($pcbFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", $pcbX),
				"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		$pcbX = (count($fritzingPins) - $pcbI - 1) * $globalInterval;
		array_push($pcbBackTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
			"tags" => $value["name"],
		));
		array_push($pcbBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", $pcbX),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$pcbI++;
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 0,
						"y" => 0,
						"width" => $pcbWidth,
						"height" => $pcbHeight,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $pcbTextSize,
						"font-family" => $globalFontFamily,
						"text-anchor" => "middle",
						"fill" => $pcbSilkscreenColor,
						"stroke" => "none",
						"transform" => sprintf("translate(%s,%s)", ($pcbWidth - (count($fritzingPins) / 2 - 1) * $globalInterval) / 2, $pcbHeight / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / 2 - $globalInterval),
							),
							"tags" => $pcbFrontTextsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / -2 + $globalInterval + $pcbTextSize - fontBaselineOffset($pcbTextSize)),
							),
							"tags" => $pcbBackTextsChain,
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => $pcbBackgroundColor,
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", ($breadboardWidth - (count($fritzingPins) / 2 - 1) * $globalInterval) / 2, $pcbHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($pcbHeight - $globalInterval) / 2),
									),
									"tags" => $pcbFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", ($pcbHeight - $globalInterval) / -2),
									),
									"tags" => $pcbBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicColumnLinesChain = array();
$schematicRowLinesChain = array();
$schematicJointsColumnChain = array();
$schematicJointsRowChain = array();
$schematicLEDsColumnChain = array();
$schematicLEDsRowChain = array();
$schematicPinsColumnChain = array();
$schematicPinsRowChain = array();
$schematicTerminalsColumnChain = array();
$schematicTerminalsRowChain = array();
$schematicTextsColumnChain = array();
$schematicTextsRowChain = array();
$schematicI = 0;
foreach ($globalPins["C"] as $key => $value) {
	array_push($schematicColumnLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#column-line-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicJointsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0) rotate(-45)%s", $schematicI * 30, (($globalDirection) ? "" : " scale(1,-1)")),
		),
	));
	array_push($schematicPinsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0) rotate(90)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicTextsColumnChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
		"tags" => $key + 1,
	));
	$schematicI++;
}
$schematicI = 0;
foreach ($globalPins["R"] as $key => $value) {
	array_push($schematicRowLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#row-line-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicJointsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicPinsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTextsRowChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
		"tags" => $key + 1,
	));
	$schematicI++;
}
$schematicI = 0;
foreach ($globalPins["E"] as $key => $value) {
	array_push($schematicPinsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-pin", $value),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-%s-terminal", $value),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTextsRowChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
			"xml:space" => "preserve",
		),
		"tags" => (($value == "NC") ? "" : " ," . ($key + 1)),
	));
	$schematicI++;
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-a-template",
						"points" => "0,0 4,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-b-template",
						"points" => "4,0 4,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-c-template",
						"points" => "4,4 4,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-d-template",
						"points" => "4,8 0,8",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-e-template",
						"points" => "0,8 0,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-f-template",
						"points" => "0,4 0,0",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "segment-g-template",
						"points" => "0,4 4,4",
						"stroke-linecap" => "round",
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "segment-p-template",
						"cx" => 6,
						"cy" => 8,
						"r" => $schematicBorderThickness / 2,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-template",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicBorderThickness,
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-a-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-d-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-g-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-template",
								"fill" => $schematicPinColor,
								"stroke" => "none",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-a-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-a-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-b-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-c-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-d-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-d-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-e-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-f-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-g-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-g-template",
								"stroke" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "segment-p-highlight",
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-template",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-template",
								"fill" => $schematicBorderColor,
							),
						),
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "row-line-template",
						"points" => sprintf("0,0 %s,0", (count($globalPins["C"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "column-line-template",
						"points" => sprintf("0,0 0,%s", (count($globalPins["R"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "joint-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 2,
						"fill" => $schematicBorderColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "joint-row-template",
					),
					"tags" => $schematicJointsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "direction-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-1,0 1,0 0,2",
								"fill" => $schematicBorderColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-2 0,0",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness / 2,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-4,-5 4,-5 0,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-10 0,10",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "-4,5 4,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(-3,8)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,0) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,2) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-row-template",
					),
					"tags" => $schematicLEDsColumnChain,
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicColumnLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicRowLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(27.5,7.5)",
					),
					"tags" => $schematicLEDsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight - 10),
					),
					"tags" => $schematicPinsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(10,0)",
					),
					"tags" => $schematicPinsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight),
					),
					"tags" => $schematicTerminalsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(0,0)",
					),
					"tags" => $schematicTerminalsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(20,%s)", $schematicHeight - 20),
					),
					"tags" => array(
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-a-highlight",
								"transform" => "translate(0,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-b-highlight",
								"transform" => "translate(30,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-c-highlight",
								"transform" => "translate(60,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-d-highlight",
								"transform" => "translate(90,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-e-highlight",
								"transform" => "translate(120,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-f-highlight",
								"transform" => "translate(150,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-g-highlight",
								"transform" => "translate(180,0)",
							),
						),
						array(
							"name" => "use",
							"attributes" => array(
								"xlink:href" => "#segment-p-highlight",
								"transform" => "translate(210,0)",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $schematicTextSize,
						"font-family" => $globalFontFamily,
						"fill" => $schematicTextColor,
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "end",
								"transform" => sprintf("translate(33,%s)", $schematicHeight - 2),
							),
							"tags" => $schematicTextsColumnChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "start",
								"transform" => "translate(2,10)",
							),
							"tags" => $schematicTextsRowChain,
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}