#!/bin/bash
for asd in Red Orange Yellow Green Cyan Blue Purple White; do
COLOR="${asd}"

# Do not modify the code below
PREFIX="led"

ICON="icon"
BREADBOARD="breadboard"
SCHEMATIC="schematic"
PCB="pcb"
PART="part"
DIST="dist"

mkdir "${ICON}"
mkdir "${BREADBOARD}"
mkdir "${SCHEMATIC}"
mkdir "${PCB}"
mkdir "${PART}"
mkdir "${DIST}"

UUID=`uuidgen`
part=`cat "${PART}.${PREFIX}-template.fzp"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{UUID\\\\}/${UUID}/g"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{COLOR\\\\}/${COLOR}/g"`

breadboard=`cat "svg.${BREADBOARD}.${PREFIX}-template.svg"`
breadboard=`echo "${breadboard}" | sed -r "s/\\\\$\\\\{COLOR\\\\}/${COLOR}/g"`

icon=`cat "svg.${ICON}.${PREFIX}-template.svg"`
icon=`echo "${icon}" | sed -r "s/\\\\$\\\\{COLOR\\\\}/${COLOR}/g"`

schematic=`cat "svg.${SCHEMATIC}.${PREFIX}-template.svg"`

pcb=`cat "svg.${PCB}.${PREFIX}-template.svg"`

FILENAME=`echo "${PREFIX}-${COLOR}" | tr "[[:upper:]]" "[[:lower:]]"`
part=`echo "${part}" | sed -r "s/\\\\$\\\\{FILENAME\\\\}/${FILENAME}/g"`

echo -n "${icon}" >"${ICON}/${FILENAME}.svg"
echo -n "${breadboard/\$\{CHAIN\}/${breadboard_g}}" >"${BREADBOARD}/${FILENAME}.svg"
echo -n "${schematic}" >"${SCHEMATIC}/${FILENAME}.svg"
echo -n "${pcb}" >"${PCB}/${FILENAME}.svg"
echo -n "${part/\$\{CHAIN\}/${part_g}}" >"${PART}/${FILENAME}.fzp"

ln -s "${ICON}/${FILENAME}.svg" "svg.${ICON}.${FILENAME}.svg"
ln -s "${BREADBOARD}/${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg"
ln -s "${SCHEMATIC}/${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg"
ln -s "${PCB}/${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg"
ln -s "${PART}/${FILENAME}.fzp" "${PART}.${FILENAME}.fzp"

zip \
"${DIST}/${FILENAME}.fzpz" \
"svg.${ICON}.${FILENAME}.svg" \
"svg.${BREADBOARD}.${FILENAME}.svg" \
"svg.${SCHEMATIC}.${FILENAME}.svg" \
"svg.${PCB}.${FILENAME}.svg" \
"${PART}.${FILENAME}.fzp" \

rm "svg.${ICON}.${FILENAME}.svg" "svg.${BREADBOARD}.${FILENAME}.svg" "svg.${SCHEMATIC}.${FILENAME}.svg" "svg.${PCB}.${FILENAME}.svg" "${PART}.${FILENAME}.fzp"
done