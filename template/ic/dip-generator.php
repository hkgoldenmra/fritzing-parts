<?php

// global settings
$globalRows = 4;
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";
include_once("template/ic-dip-CD4011.php");

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "IC";
$fritzingTags = array("IC", "DIP");
$fritzingProperties = array(
	"family" => "A IC",
	"package" => "DIP",
	"pins" => count($globalPins),
	"type" => $globalTitle,
);

// icon settings
$iconUnit = "in";
$iconPinColor = "#888888";
$iconPinThickness = 0;
$iconShieldColor = "#333333";
$iconPokeColor = "#000000";
$icon1stPinColor = "#000000";
$iconTitleColor = "#FFFFFF";
$iconTitleSize = 5;

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardPinColor = $iconPinColor;
$breadboardPinThickness = $iconPinThickness;
$breadboardShieldColor = $iconShieldColor;
$breadboardPokeColor = $iconPokeColor;
$breadboard1stPinColor = $icon1stPinColor;
$breadboardTitleColor = $iconTitleColor;
$breadboardTitleSize = $iconTitleSize;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTitleSize = 5;
$pcbTextSize = 2;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematic1stPinColor = $schematicBorderColor;
$schematicTitleColor = $schematicBorderColor;
$schematicTitleSize = 5;
$schematicTextColor = $schematicPinColor;
$schematicTextSize = 3;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 4;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

// debug
$debugMinRows = 4;
if ($globalRows < $debugMinRows) {
	$globalRows = $debugMinRows;
}

$globalInterval = 10;
$globalDigit = count($globalPins) / 2;
$iconWidth = 50;
$iconHeight = 50;
$breadboardWidth = $globalDigit * $globalInterval;
$breadboardHeight = ($globalRows - 1) * $globalInterval;
$pcbWidth = $globalRows * $globalInterval;
$pcbHeight = $globalDigit * $globalInterval;
$schematicWidth = 80;
$schematicHeight = ($globalDigit + 1) * $globalInterval;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("ic-dip-%s", $globalTitle);
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
foreach ($globalPins as $key => $value) {
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => sprintf("connector-%s", $key),
			"name" => $key,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => $value,
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
//									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
//									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
		array(
			"name" => "description",
			"tags" => $globalDescription,
		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$iconPinsChain = array();
$iconRightPinsChain = array();
for ($iconI = 0; $iconI < $iconWidth / 10; $iconI++) {
	$iconY = $iconI * $globalInterval;
	array_push($iconPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#front-leg-template",
			"transform" => sprintf("translate(%s,0)", $iconY),
		),
	));
	array_push($iconRightPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#back-leg-template",
			"transform" => sprintf("translate(%s,0)", $iconY),
		),
	));
}
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%sin", $iconWidth / $multiple),
		"height" => sprintf("%sin", $iconHeight / $multiple),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "leg-template",
						"points" => "-5,0 5,0",
						"fill" => "none",
						"stroke" => $iconPinColor,
						"stroke-width" => $iconPinThickness,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $iconHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "rect",
									"attributes" => array(
										"x" => 0,
										"y" => ($iconHeight - 10) / -2,
										"width" => $iconWidth,
										"height" => $iconHeight - 10,
										"fill" => $iconShieldColor,
									),
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => 0,
										"cy" => 0,
										"r" => 5,
										"fill" => $iconPokeColor,
									),
								),
								array(
									"name" => "text",
									"attributes" => array(
										"x" => 5,
										"y" => fontBaselineOffset($iconTitleSize),
										"fill" => $iconTitleColor,
										"font-family" => $globalFontFamily,
										"font-size" => $iconTitleSize,
									),
									"tags" => $globalTitle,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(5,0)"
									),
									"tags" => array(
										array(
											"name" => "circle",
											"attributes" => array(
												"cx" => 0,
												"cy" => $iconHeight / 2 - 7.5,
												"r" => 1,
												"fill" => $icon1stPinColor,
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $iconHeight / 2 - 5),
											),
											"tags" => $iconPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $iconHeight / -2 + 5),
											),
											"tags" => $iconRightPinsChain,
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardFrontPinsChain = array();
$breadboardFrontTerminalsChain = array();
$breadboardBackPinsChain = array();
$breadboardBackTerminalsChain = array();
$breadboardI = 0;
foreach ($globalPins as $key => $value) {
	if ($breadboardI < $globalDigit) {
		$breadboardY = $breadboardI * $globalInterval;
		array_push($breadboardFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#front-pin-template",
				"transform" => sprintf("translate(%s,0)", $breadboardY),
			),
		));
		array_push($breadboardFrontTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $breadboardY),
			),
		));
	} else {
		$breadboardY = $breadboardWidth + ($globalDigit - $breadboardI - 1) * $globalInterval;
		array_push($breadboardBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#back-pin-template",
				"transform" => sprintf("translate(%s,0)", $breadboardY),
			),
		));
		array_push($breadboardBackTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $breadboardY),
			),
		));
	}
	$breadboardI++;
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%sin", $breadboardWidth / $multiple),
		"height" => sprintf("%sin", $breadboardHeight / $multiple),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "front-pin-template",
						"points" => "-2.5,0 -2.5,1 -0.5,2 -0.5,5 0.5,5 0.5,2 2.5,1 2.5,0",
						"fill" => $breadboardPinColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "polygon",
					"attributes" => array(
						"id" => "back-pin-template",
						"points" => "-2.5,0 -2.5,-1 -0.5,-2 -0.5,-5 0.5,-5 0.5,-2 2.5,-1 2.5,0",
						"fill" => $breadboardPinColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "rect",
									"attributes" => array(
										"x" => 0,
										"y" => ($breadboardHeight - 10) / -2,
										"width" => $breadboardWidth,
										"height" => $breadboardHeight - 10,
										"fill" => $breadboardShieldColor,
									),
								),
								array(
									"name" => "path",
									"attributes" => array(
										"transform" => "translate(0,0)",
										"d" => "M 5,0 C 5,2.7614237 2.7614237,5 0,5 V -5 c 2.7614237,0 5,2.2385763 5,5 z",
										"fill" => $breadboardPokeColor,
									),
								),
								array(
									"name" => "text",
									"attributes" => array(
										"x" => $breadboardWidth / 2,
										"y" => fontBaselineOffset($breadboardTitleSize),
										"fill" => $breadboardTitleColor,
										"font-family" => $globalFontFamily,
										"font-size" => $breadboardTitleSize,
										"text-anchor" => "middle",
									),
									"tags" => $globalTitle,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(5,0)",
									),
									"tags" => array(
										array(
											"name" => "circle",
											"attributes" => array(
												"cx" => 0,
												"cy" => $breadboardHeight / 2 - 7.5,
												"r" => 1,
												"fill" => $breadboard1stPinColor,
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2 - 5),
											),
											"tags" => $breadboardFrontPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2),
											),
											"tags" => $breadboardFrontTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2 + 5),
											),
											"tags" => $breadboardBackPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2),
											),
											"tags" => $breadboardBackTerminalsChain,
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontPinsChain = array();
$pcbFrontTextsChain = array();
$pcbBackPinsChain = array();
$pcbBackTextsChain = array();
$pcbI = 0;
foreach ($globalPins as $key => $value) {
	if ($pcbI < $globalDigit) {
		$pcbX = $pcbI * $globalInterval;
		array_push($pcbFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
				"transform" => sprintf("translate(0,%s)", $pcbX),
			),
		));
		array_push($pcbFrontTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $pcbX,
			),
			"tags" => $key,
		));
	} else {
		$pcbX = $pcbHeight + ($globalDigit - $pcbI - 1) * $globalInterval;
		array_push($pcbBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
				"transform" => sprintf("translate(0,%s)", $pcbX),
			),
		));
		array_push($pcbBackTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $pcbX,
			),
			"tags" => $key,
		));
	}
	$pcbI++;
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%sin", $pcbWidth / $multiple),
		"height" => sprintf("%sin", $pcbHeight / $multiple),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"points" => sprintf('%3$s,0 0,0 0,%2$s %1$s,%2$s %1$s,0 %4$s,0', $pcbWidth, $pcbHeight, $pcbWidth / 2 - 5, $pcbWidth / 2 + 5),
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,0)", $pcbWidth / 2),
						"fill" => $pcbSilkscreenColor,
						"stroke" => "none",
						"font-family" => $globalFontFamily,
					),
					"tags" => array(
						array(
							"name" => "text",
							"attributes" => array(
								"font-size" => $pcbTitleSize,
								"text-anchor" => "middle",
								"transform" => "rotate(90)",
								"x" => $pcbHeight / 2,
								"y" => fontBaselineOffset($pcbTitleSize),
							),
							"tags" => $globalTitle,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"font-size" => $pcbTextSize,
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,%s)", $pcbWidth / -2 + 10, fontBaselineOffset($pcbTextSize) + 5),
										"text-anchor" => "start",
									),
									"tags" => $pcbFrontTextsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,%s)", $pcbWidth / 2 - 10, fontBaselineOffset($pcbTextSize) + 5),
										"text-anchor" => "end",
									),
									"tags" => $pcbBackTextsChain,
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => "none",
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => "none",
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => "none",
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										)
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,5)", $pcbWidth / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $pcbWidth / -2 + 5),
									),
									"tags" => $pcbFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $pcbWidth / 2 - 5),
									),
									"tags" => $pcbBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicLeftPinsChain = array();
$schematicLeftTerminalsChain = array();
$schematicLeftTextsChain = array();
$schematicRightPinsChain = array();
$schematicRightTerminalsChain = array();
$schematicRightTextsChain = array();
$schematicI = 0;
foreach ($globalPins as $key => $value) {
	if ($schematicI < $globalDigit) {
		$schematicY = $schematicI * $globalInterval;
		array_push($schematicLeftPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicLeftTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicLeftTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	} else {
		$schematicY = $schematicHeight - 10 + ($globalDigit - $schematicI - 1) * $globalInterval;
		array_push($schematicRightPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicRightTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicRightTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	}
	$schematicI++;
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%sin", $schematicWidth / $multiple),
		"height" => sprintf("%sin", $schematicHeight / $multiple),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 20,
						"y" => 0,
						"width" => $schematicWidth - 40,
						"height" => $schematicHeight,
						"fill" => $schematicBackgroundColor,
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"cx" => 25,
						"cy" => 10,
						"r" => 1,
						"fill" => $schematic1stPinColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,10)", $schematicWidth / 2),
					),
					"tags" => array(
						array(
							"name" => "text",
							"attributes" => array(
								"fill" => $schematicTitleColor,
								"stroke" => "none",
								"font-family" => $globalFontFamily,
								"font-size" => $schematicTitleSize,
								"text-anchor" => "middle",
								"transform" => "rotate(90)",
								"x" => count($globalPins) * 2.5 - 5,
								"y" => fontBaselineOffset($schematicTitleSize),
							),
							"tags" => $globalTitle,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $schematicWidth / -2 + 10),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,0)",
									),
									"tags" => $schematicLeftPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(-10,0)",
									),
									"tags" => $schematicLeftTerminalsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(5,%s)", -fontBaselineOffset($schematicTextSize) * 2),
										"fill" => $schematicTextColor,
										"font-family" => $globalFontFamily,
										"font-size" => $schematicTextSize,
										"stroke" => "none",
										"text-anchor" => "end",
									),
									"tags" => $schematicLeftTextsChain,
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $schematicWidth / 2 - 10),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(0,0)",
									),
									"tags" => $schematicRightPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(10,0)",
									),
									"tags" => $schematicRightTerminalsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(-5,%s)", -fontBaselineOffset($schematicTextSize) * 2),
										"fill" => $schematicTextColor,
										"font-family" => $globalFontFamily,
										"font-size" => $schematicTextSize,
										"stroke" => "none",
										"text-anchor" => "start",
									),
									"tags" => $schematicRightTextsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}