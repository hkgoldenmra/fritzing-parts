<?php

$globalTitle = "555";
$globalDescription = "The 555 timer IC is an integrated circuit (chip) used in a variety of timer, pulse generation, and oscillator applications. The 555 can be used to provide time delays, as an oscillator, and as a flip-flop element.";
$globalPins = array(
	"GND" => "Ground",
	"TRIG" => "When this pin lower than 1/3 CTRL, output high voltage",
	"OUT" => "Output VCC or GND Voltage Level",
	"RST" => "Active or Reset (Active when High)",
	"CTRL" => "Refer to the Control Voltage (Refer to VCC if no connection)",
	"THRE" => "When this pin higher than 2/3 CTRL, output low voltage",
	"DIS" => "Discharge the capacitor between intervals",
	"VCC" => "Positive Power Supply (4.5V ~ 15V)",
);
