<?php

$globalTitle = "ATmega328P";
$globalDescription = "The ATmega328 is a single-chip microcontroller created by Atmel in the megaAVR family (later Microchip Technology acquired Atmel in 2016). It has a modified Harvard architecture 8-bit RISC processor core.";
$globalPins = array(
	"RST" => "Reset",
	"D0/RX" => "Digital 0, PORTD 0 / Receive Data",
	"D1/TX" => "Digital 1, PORTD 1 / Transfer Data",
	"D2" => "Digital 2, PORTD 2",
	"D3~" => "Digital 3, PORTD 3, PWM",
	"D4" => "Digital 4, PORTD 4",
	"VCC" => "Positive Supply Voltage (1.8V ~ 5.5V)",
	"GND0" => "Ground 0",
	"C0" => "Crystal 0",
	"C1" => "Crystal 1",
	"D5~" => "Digital 5, PORTD 5, PWM",
	"D6~" => "Digital 6, PORTD 6, PWM",
	"D7" => "Digital 7, PORTD 7",
	"D8" => "Digital 8, PORTB 0",
	"D9~" => "Digital 9, PORTB 1, PWM",
	"D10~" => "Digital 10, PORTB 2, PWM, SS",
	"D11~" => "Digital 11, PORTB 3, PWM, MOSI",
	"D12" => "Digital 12, PORTB 4, MISO",
	"D13" => "Digital 13, PORTB 5, SCK",
	"AVCC" => "Analog VCC",
	"AREF" => "Analog Reference",
	"GND1" => "Ground 1",
	"A0" => "Analog 0, PORTC 0",
	"A1" => "Analog 1, PORTC 1",
	"A2" => "Analog 2, PORTC 2",
	"A3" => "Analog 3, PORTC 3",
	"A4" => "Analog 4, PORTC 4, SDA",
	"A5" => "Analog 5, PORTC 5, SCL",
);
