<?php

$globalTitle = "CD4001";
$globalDescription = "The CD4001 IC contains four independent NOR gates.";
$globalPins = array(
	"I0A" => "NOR gates Input 0A",
	"I0B" => "NOR gates Input 0B",
	"O0" => "NOR gates Output 0",
	"O1" => "NOR gates Output 1",
	"I1A" => "NOR gates Input 1A",
	"I1B" => "NOR gates Input 1B",
	"GND" => "Ground",
	"I2A" => "NOR gates Input 2A",
	"I2B" => "NOR gates Input 2B",
	"O2" => "NOR gates Output 2",
	"O3" => "NOR gates Output 3",
	"I3A" => "NOR gates Input 3B",
	"I3B" => "NOR gates Input 3A",
	"VCC" => "Positve Voltage Supply (3.0V ~ 15.0V)",
);
