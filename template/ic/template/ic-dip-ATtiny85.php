<?php

$globalTitle = "ATtiny85";
$globalDescription = "ATtiny (also known as TinyAVR) are a subfamily of the popular 8-bit AVR microcontrollers, which typically has fewer features, fewer I/O pins, and less memory than other AVR series chips. The first members of this family were released in 1999 by Atmel (later acquired by Microchip Technology in 2016).";
$globalPins = array(
	"RST" => "Reset",
	"D3/A2" => "Digital 3 / Analog 2",
	"D4/A1" => "Digital 4 / Analog 1",
	"GND" => "Ground",
	"D0~" => "Digital 0, PWM, MOSI",
	"D1~" => "Digital 1, PWM, MISO",
	"D2/A0" => "Digital 2 / Analog 0, SCK",
	"VCC" => "Positive Supply Voltage (2.7V ~ 5.5V)",
);
