<?php

$globalTitle = "CD4017";
$globalDescription = "The CD4017 is a CMOS Decade counter IC. CD4017 is used for low range counting applications. It can count from 0 to 9 (the decade count). The circuit designed by using  this ic  will save board space and also time required to design the circuit. CD4017 is as 'Johnson 10 stage decade counter'.";
$globalPins = array(
	"OUT5" => "When the value of counts is 5, this pin sends high signal",
	"OUT1" => "When the value of counts is 1, this pin sends high signal",
	"OUT0" => "When the value of counts is 0, this pin sends high signal",
	"OUT2" => "When the value of counts is 2, this pin sends high signal",
	"OUT6" => "When the value of counts is 6, this pin sends high signal",
	"OUT7" => "When the value of counts is 7, this pin sends high signal",
	"OUT3" => "When the value of counts is 3, this pin sends high signal",
	"GND" => "Ground",
	"OUT8" => "When the value of counts is 8, this pin sends high signal",
	"OUT4" => "When the value of counts is 4, this pin sends high signal",
	"OUT9" => "When the value of counts is 9, this pin sends high signal",
	"CO" => "When the counts exceed 10, this pin sends high signal",
	"EN" => "Enable the IC (Active when low)",
	"SLK" => "The counter increase 1 when this pin retrieve low to high signal",
	"RST" => "Resets the counter to 0 when this pin retrieve high to low signal",
	"VCC" => "Positive Supply Voltage (3.0V ~ 15.0V)",
);
