<?php

$globalTitle = "556";
$globalDescription = "The 556 timer is a dual version of the 555 timers. In other words, it is embedded with two 555 timers operating separately. The CMOS versions offer improved characteristics for particular applications. The two timers operate independently of each other sharing only Vs and ground. The circuit may be triggered and reset on falling waveforms. The 556 timer is a 14 pin configuration is shown in the figure. Each Timer is provided with its own threshold, trigger, discharge, control, reset, and output pins. This IC can be used for both the oscillator as well as pulse generator due to the availability of two separate 555 timers.";
$globalPins = array(
	"DIS0" => "Discharge the capacitor between intervals",
	"THRE0" => "When this pin higher than 2/3 CTRL, output low voltage",
	"CTRL0" => "Refer to the Control Voltage (Refer to VCC if no connection)",
	"RST0" => "Active or Reset (Active when High)",
	"OUT0" => "Output VCC or GND Voltage Level",
	"TRIG0" => "When this pin lower than 1/3 CTRL, output high voltage",
	"GND" => "Ground",
	"TRIG1" => "When this pin lower than 1/3 CTRL, output high voltage",
	"OUT1" => "Output VCC or GND Voltage Level",
	"RST1" => "Active or Reset (Active when High)",
	"CTRL1" => "Refer to the Control Voltage (Refer to VCC if no connection)",
	"THRE1" => "When this pin higher than 2/3 CTRL, output low voltage",
	"DIS1" => "Discharge the capacitor between intervals",
	"VCC" => "Positive Power Supply (4.5V ~ 15V)",
);
