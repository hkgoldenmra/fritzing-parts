<?php

$globalTitle = "MCP23017";
$globalDescription = "The MCP23017 provides 16-bit, general purpose parallel I/O expansion for I2C bus.";
$globalPins = array(
	"DB0" => "Input / Output Data B0 (0-th bit data B)",
	"DB1" => "Input / Output Data B1 (1-st bit data B)",
	"DB2" => "Input / Output Data B2 (2-nd bit data B)",
	"DB3" => "Input / Output Data B3 (3-rd bit data B)",
	"DB4" => "Input / Output Data B4 (4-th bit data B)",
	"DB5" => "Input / Output Data B5 (5-th bit data B)",
	"DB6" => "Input / Output Data B6 (6-th bit data B)",
	"DB7" => "Input / Output Data B7 (7-th bit data B)",
	"VCC" => "Positive Power Supply (1.8V ~ 5.5V)",
	"GND" => "Ground",
	"NC0" => "No Connection 0",
	"SCL" => "Serial Clock",
	"SDA" => "Serial Data",
	"NC1" => "No Connection 1",
	"A0" => "Address Input (0-th bit Address)",
	"A1" => "Address Input (1-st bit Address)",
	"A2" => "Address Input (2-nd bit Address)",
	"RST" => "Reset (Active when Low)",
	"INTB" => "Interrupt Output B (Active when High)",
	"INTA" => "Interrupt Output A (Active when High)",
	"DA0" => "Input / Output Data A0 (0-th bit data A)",
	"DA1" => "Input / Output Data A1 (1-st bit data A)",
	"DA2" => "Input / Output Data A2 (2-nd bit data A)",
	"DA3" => "Input / Output Data A3 (3-rd bit data A)",
	"DA4" => "Input / Output Data A4 (4-th bit data A)",
	"DA5" => "Input / Output Data A5 (5-th bit data A)",
	"DA6" => "Input / Output Data A6 (6-th bit data A)",
	"DA7" => "Input / Output Data A7 (7-th bit data A)",
);
