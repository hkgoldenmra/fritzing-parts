<?php

$globalTitle = "CD4011";
$globalDescription = "The CD4011 IC contains four independent NAND gates.";
$globalPins = array(
	"I0A" => "NAND gates Input 0A",
	"I0B" => "NAND gates Input 0B",
	"O0" => "NAND gates Output 0",
	"O1" => "NAND gates Output 1",
	"I1A" => "NAND gates Input 1A",
	"I1B" => "NAND gates Input 1B",
	"GND" => "Ground",
	"I2A" => "NAND gates Input 2A",
	"I2B" => "NAND gates Input 2B",
	"O2" => "NAND gates Output 2",
	"O3" => "NAND gates Output 3",
	"I3A" => "NAND gates Input 3B",
	"I3B" => "NAND gates Input 3A",
	"VCC" => "Positve Voltage Supply (3.0V ~ 15.0V)",
);
