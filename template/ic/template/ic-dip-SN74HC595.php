<?php

$globalTitle = "SN74HC595";
$globalDescription = "The SN74HC595 devices contain an 8-bit, serial-in, parallel-out shift register that feeds an 8-bit D-type storage register. The storage register has parallel 3-state outputs. Separate clocks are provided for both the shift and storage register.";
$globalPins = array(
	"D6" => "Parallel Data Output (6-th bit data)",
	"D5" => "Parallel Data Output (5-th bit data)",
	"D4" => "Parallel Data Output (4-th bit data)",
	"D3" => "Parallel Data Output (3-rd bit data)",
	"D2" => "Parallel Data Output (2-nd bit data)",
	"D1" => "Parallel Data Output (1-st bit data)",
	"D0" => "Parallel Data Output (0-th bit data)",
	"GND" => "Ground",
	"SDO" => "Serial Data Output",
	"CLR" => "Clear (Active when High)",
	"SCK" => "Serial Clock (Period Low to High)",
	"LCK" => "Latch Clock (Period Low to High)",
	"EN" => "Enable (Active when Low)",
	"SDI" => "Serial Data Input",
	"D7" => "Parallel Data Output (7-th bit data)",
	"VCC" => "Positive Supply Voltage (2V ~ 6V)",
);
