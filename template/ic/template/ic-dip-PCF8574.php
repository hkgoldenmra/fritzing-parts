<?php

$globalTitle = "PCF8574";
$globalDescription = "The PCF8574(A) provides general-purpose remote I/O expansion via the two-wire bidirectional I2C-bus (serial clock (SCL), serial data (SDA)). The devices consist of eight quasi-bidirectional ports, 100 kHz I2C-bus interface, three hardware address inputs and interrupt output operating between 2.5V and 6V.";
$globalPins = array(
	"A0" => "Address Input 0 (0-th bit address)",
	"A1" => "Address Input 1 (1-st bit address)",
	"A2" => "Address Input 2 (2-nd bit address)",
	"D0" => "Input / Output Data 0 (0-th bit data)",
	"D1" => "Input / Output Data 1 (1-st bit data)",
	"D2" => "Input / Output Data 2 (2-nd bit data)",
	"D3" => "Input / Output Data 3 (3-rd bit data)",
	"VCC" => "Positive Power Supply (2.5V ~ 6.0V)",
	"D4" => "Input / Output Data 4 (4-th bit data)",
	"D5" => "Input / Output Data 5 (5-th bit data)",
	"D6" => "Input / Output Data 6 (6-th bit data)",
	"D7" => "Input / Output Data 7 (7-th bit data)",
	"INT" => "Interrupt Output (Active when Low)",
	"SCL" => "Serial Clock",
	"SDA" => "Serial Data",
	"GND" => "Ground",
);
