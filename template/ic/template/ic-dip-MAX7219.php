<?php

$globalTitle = "MAX7219";
$globalDescription = "The MAX7219 is compact, serial input/output common-cathode display drivers that interface microprocessors to 7-segment numeric LED displays of up to 8 digits, bar-graph displays, or 64 individual LEDs. Included on-chip are a BCD code-B decoder, multiplex scan circuitry, segment and digit drivers, and an 8x8 static RAM that stores each digit.";
$globalPins = array(
	"SDI" => "Serial Data Input",
	"K0" => "0-th Cathode",
	"K4" => "4-th Cathode",
	"GND0" => "Ground 0",
	"K6" => "6-th Cathode",
	"K2" => "2-nd Cathode",
	"K3" => "3-rd Cathode",
	"K7" => "7-th Cathode",
	"GND1" => "Ground 1",
	"K5" => "5-th Cathode",
	"K1" => "1-st Cathode",
	"CS" => "Chip Select (Active when Low)",
	"SCK" => "Serial Clock (Period High to Low)",
	"A1" => "1-st Anode",
	"A6" => "6-th Anode",
	"A2" => "2-nd Anode",
	"A7" => "7-th Anode",
	"ISET" => "Connect to Ground through a resistor to set the peak of current of the anodes",
	"VCC" => "Positive Voltage Supply (4V ~ 5.5V)",
	"A3" => "3-rd Anode",
	"A5" => "5-th Anode",
	"A0" => "0-th Anode",
	"A4" => "4-th Anode",
	"SDO" => "Serial Data Output",
);
