<?php

$globalTitle = "FT232RL";
$globalDescription = "The FT232R is a USB to serial UART interface with optional clock generator output. USB to serial designs using the FT232R have been further simplified by fully integrating the external EEPROM, clock circuit and USB resistors onto the device.";
$globalPins = array(
	"TXD" => "Transmit Asynchronous Data Output",
	"DTR" => "Data Terminal Ready Control Output",
	"RTS" => "Request to Send Control Output",
	"VCCIO" => "1.8V to 5.25V supply to the UART Interface and CBUS",
	"RXD" => "Receiving Asynchronous Data Input",
	"RI" => "Ring Indicator Control Input",
	"GND0" => "Ground 0",
	"NC0" => "No Connection 0",
	"DSR" => "Data Set Ready Control Input",
	"DCD" => "Data Carrier Detect Control Input",
	"CTS" => "Clear To Send Control Input",
	"CBUS4" => "Configurable CBUS output only",
	"CBUS2" => "Factory default configuration is TXDEN",
	"CBUS3" => "Factory default configuration is PWREN",
	"USBD+" => "USB Data+, incorporating internal series resistor and 1.5K ohms pull up resistor to 3.3V.",
	"USBD-" => "USB Data-, incorporating internal series resistor.",
	"3V3" => "3.3V output from integrated LDO regulator.",
	"GND1" => "Ground 1",
	"RST" => "Reset the FT232R by external device (Active when Low)",
	"VCC" => "Positive Power Supply (3.3V ~ 5.25V)",
	"GND2" => "Ground 2",
	"CBUS1" => "Factory default configuration is RXLED",
	"CBUS0" => "Factory default configuration is TXLED",
	"NC1" => "No Connection 1",
	"AGND" => "Analog Ground for internal clock multiplier",
	"TEST" => "Puts the device into IC test mode (Active when Low)",
	"OSCI" => "12MHz Oscillator Cell Input",
	"OSCO" => "12MHz Oscillator Cell Output",
);
