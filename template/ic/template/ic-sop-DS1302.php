<?php

$globalTitle = "DS1302";
$globalDescription = "The DS1302 trickle-charge timekeeping chip contains a real-time clock/calendar and 31 bytes of static RAM. It communicates with a microprocessor via a simple serial interface. The real-time clock/calendar provides seconds, minutes, hours, day, date, month, and year information. The end of the month date is automatically adjusted formonths with fewer than 31 days, including corrections for leap year. The clock operates in either the 24-hour or 12-hour format with an AM/PM indicator.";
$globalPins = array(
	"VCC0" => "Primary Positive Power Supply (2.0V ~ 5.5V)",
	"C0" => "Standard 32768 Hz Quartz Crystal 0",
	"C1" => "Standard 32768 Hz Quartz Crystal 1",
	"GND" => "Ground",
	"RST" => "High as Read/Write, Low as Reset",
	"DAT" => "MSB 8-bit Command/Data",
	"CLK" => "Sends Command/Data on the Serial Interface",
	"VCC1" => "Secondary Positive Power Supply (2.0V ~ 5.5V)",
);
