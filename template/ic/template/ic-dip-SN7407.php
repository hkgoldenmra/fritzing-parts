<?php

$globalTitle = "SN7407";
$globalDescription = "These TTL hex buffers feature high-voltage open-collector outputs for interfacing with high-level circuits (such  as  MOS) or for driving high-current loads (such as lamps or relays) and also are characterized for use as buffers for driving TTL inputs. The SN7407 has minimum breakdown voltages of 30V. The maximum sink current is 40mA for the SN7407.";
$globalPins = array(
	"I0" => "BUFFER Gate Input 0",
	"O0" => "BUFFER Gate Output 0",
	"I1" => "BUFFER Gate Input 1",
	"O1" => "BUFFER Gate Output 1",
	"I2" => "BUFFER Gate Input 2",
	"O2" => "BUFFER Gate Output 2",
	"GND" => "Ground",
	"O3" => "BUFFER Gate Input 3",
	"I3" => "BUFFER Gate Output 3",
	"O4" => "BUFFER Gate Input 4",
	"I4" => "BUFFER Gate Output 4",
	"O5" => "BUFFER Gate Input 5",
	"I5" => "BUFFER Gate Output 5",
	"VCC" => "Positive Supply Voltage (4.5V ~ 5.5V)",
);
