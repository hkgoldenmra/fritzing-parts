<?php

$globalTitle = "LM3914";
$globalDescription = "The LM3914 is a mono lithic integrated circuit that senses analog voltage levels and drives 10 LEDs, providing a linear analog display. A single pin changes the display from a moving dot to a bar graph. Current drive to the LEDs is regulated and programmable, eliminating the need for resistors. This feature is one that allows operation of the whole system from less than 3V.";
$globalPins = array(
	"LED0" => "Corresponding to the Voltage Level between 10% to 19% with 1000 Ohms internal resistor",
	"GND" => "Ground",
	"VCC" => "Positive Supply Voltage (3.0V ~ 25.0V)",
	"REFLO" => "Reference Low, define the Lowest Voltage Level",
	"SIG" => "Signal, the Input Voltage Level",
	"REFHI" => "Reference High, define the Highest Voltage Level (Maximum 3V)",
	"REFOUT" => "Reference Output Voltage",
	"REFADJ" => "Reference Adjust Voltage",
	"MODE" => "Bar Mode when High, Dot Mode when Low",
	"LED9" => "Corresponding to the Voltage Level between 100% with 1000 Ohms internal resistor",
	"LED8" => "Corresponding to the Voltage Level between 90% to 99% with 1000 Ohms internal resistor",
	"LED7" => "Corresponding to the Voltage Level between 80% to 89% with 1000 Ohms internal resistor",
	"LED6" => "Corresponding to the Voltage Level between 70% to 79% with 1000 Ohms internal resistor",
	"LED5" => "Corresponding to the Voltage Level between 60% to 69% with 1000 Ohms internal resistor",
	"LED4" => "Corresponding to the Voltage Level between 50% to 59% with 1000 Ohms internal resistor",
	"LED3" => "Corresponding to the Voltage Level between 40% to 49% with 1000 Ohms internal resistor",
	"LED2" => "Corresponding to the Voltage Level between 30% to 39% with 1000 Ohms internal resistor",
	"LED1" => "Corresponding to the Voltage Level between 20% to 29% with 1000 Ohms internal resistor",
);
