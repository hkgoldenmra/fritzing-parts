<?php

$globalTitle = "HEF4094BP";
$globalDescription = "The HEF4094B is an 8-stage serial shift register. It has a storage latch associated with each stage for strobing data from the serial input to parallel buffered 3‑state outputs D0 to D7. The parallel outputs may be connected directly to common bus lines. Data is shifted on positive‑going clock transitions.";
$globalPins = array(
	"LCK" => "Latch Clock (Period Low to High)",
	"SDI" => "Serial Data Input",
	"SCK" => "Serial Clock (Period Low to High)",
	"D7" => "Parallel Data Output (7-th bit data)",
	"D6" => "Parallel Data Output (6-th Bit data)",
	"D5" => "Parallel Data Output (5-th Bit data)",
	"D4" => "Parallel Data Output (4-th Bit data)",
	"GND" => "Ground",
	"SDO" => "Serial Data Output",
	"CLR" => "Clear (Active when High)",
	"D0" => "Parallel Data Output (0-th Bit data)",
	"D1" => "Parallel Data Output (1-st Bit data)",
	"D2" => "Parallel Data Output (2-nd Bit data)",
	"D3" => "Parallel Data Output (3-rd Bit data)",
	"EN" => "Enable (Active when Low)",
	"VCC" => "Positive Supply Voltage (3V ~ 15V)",
);
