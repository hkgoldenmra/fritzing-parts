<?php

// global settings
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";
include_once("template/ic-qfp-FT232RQ.php");

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "IC";
$fritzingTags = array("IC", "QFP");
$fritzingProperties = array(
	"family" => "A IC",
	"package" => "QFP",
	"pins" => count($globalPins),
	"type" => $globalTitle,
);

// icon settings
$iconUnit = "in";
$iconPinColor = "#888888";
$iconPinThickness = 0.5;
$iconShieldColor = "#333333";
$iconPokeColor = "#000000";
$icon1stPinColor = "#000000";
$iconTitleColor = "#FFFFFF";
$iconTitleSize = 2;

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardPinColor = $iconPinColor;
$breadboardPinThickness = $iconPinThickness;
$breadboardTerminalSize = 3;
$breadboardTerminalColor = "#888800";
$breadboardBackgroundColor = "#004400";
$breadboardShieldColor = $iconShieldColor;
$breadboardPokeColor = $iconPokeColor;
$breadboard1stPinColor = $icon1stPinColor;
$breadboardTitleColor = $iconTitleColor;
$breadboardTitleSize = $iconTitleSize;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 5;
$pcbPinHeight = 1.25;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 0.5;
$pcbTitleSize = 2.5;
$pcbTextSize = 1;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematic1stPinColor = $schematicBorderColor;
$schematicTitleColor = $schematicBorderColor;
$schematicTitleSize = 5;
$schematicTextColor = $schematicPinColor;
$schematicTextSize = 3;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 4;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$iconWidth = 30;
$iconHeight = 30;
$breadboardWidth = ($globalRows + $globalColumns) * $globalInterval;
$breadboardHeight = (3 + ceil($globalRows / 4)) * $globalInterval;
$pcbWidth = $globalColumns * $globalInterval / 4 + 10;
$pcbHeight = $globalRows * $globalInterval / 4 + 10;
$pcbInterval = 2.5;
$schematicWidth = ($globalColumns + 5) * $globalInterval;
$schematicHeight = ($globalRows + 5) * $globalInterval;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("ic-qfp-%s", $globalTitle);
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
foreach ($globalPins as $key => $value) {
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => sprintf("connector-%s", $key),
			"name" => $key,
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => $value,
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
//									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("connector-%s-pin", $key),
									"terminalId" => sprintf("connector-%s-terminal", $key),
//									"legId" => sprintf("connector-%s-leg", $key),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
		array(
			"name" => "description",
			"tags" => $globalDescription,
		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$iconPinsChain = array();
for ($iconI = 0; $iconI < ($iconWidth - 10) / 10; $iconI += 0.5) {
	$iconY = $iconI * $globalInterval;
	array_push($iconPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $iconY),
		),
	));
}
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "0,-2.5 0,2.5",
						"fill" => "none",
						"stroke" => $iconPinColor,
						"stroke-width" => $iconPinThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "sip-template",
					),
					"tags" => $iconPinsChain,
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 5,
								"y" => 5,
								"width" => $iconWidth - 10,
								"height" => $iconHeight - 10,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $iconWidth / 2, $iconHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "text",
									"attributes" => array(
										"x" => 0,
										"y" => fontBaselineOffset($iconTitleSize),
										"fill" => $iconTitleColor,
										"font-family" => $globalFontFamily,
										"font-size" => $iconTitleSize,
										"text-anchor" => "middle",
									),
									"tags" => $globalTitle,
								),
								array(
									"name" => "circle",
									"attributes" => array(
										"cx" => -7.5,
										"cy" => -7.5,
										"r" => 0.5,
										"fill" => $iconPokeColor,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "dip-template",
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(-7.5,0)",
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"transform" => "translate(0,12.5)",
														"xlink:href" => "#sip-template",
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"transform" => "translate(0,-12.5)",
														"xlink:href" => "#sip-template",
													),
												),
											),
										),
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "rotate(90)",
										"xlink:href" => "#dip-template",
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardRowPinsChain = array();
$breadboardColumnPinsChain = array();
$breadboardFrontTerminalsChain = array();
$breadboardBackTerminalsChain = array();
for ($breadboardI = 0; $breadboardI < $globalRows; $breadboardI++) {
	array_push($breadboardRowPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s) rotate(90)", $breadboardI * 2.5),
		),
	));
}
for ($breadboardY = 0; $breadboardY < $globalColumns; $breadboardY++) {
	array_push($breadboardColumnPinsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0)", $breadboardY * 2.5),
		),
	));
}
$breadboardI = 0;
foreach ($globalPins as $key => $value) {
	if ($breadboardI < count($globalPins) / 2) {
		$breadboardX = $breadboardI * $globalInterval;
		array_push($breadboardFrontTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $breadboardX),
			),
		));
	} else {
		$breadboardX = (count($globalPins) - $breadboardI - 1) * 10;
		array_push($breadboardBackTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $breadboardX),
			),
		));
	}
	$breadboardI++;
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "0,-2.5 0,2.5",
						"fill" => "none",
						"stroke" => $breadboardPinColor,
						"stroke-width" => $breadboardPinThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "row-chain-template",
					),
					"tags" => $breadboardRowPinsChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "column-chain-template",
					),
					"tags" => $breadboardColumnPinsChain,
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "terminal-template",
						"x" => -$breadboardTerminalSize / 2,
						"y" => -$breadboardTerminalSize / 2,
						"width" => $breadboardTerminalSize,
						"height" => $breadboardTerminalSize,
						"fill" => $breadboardTerminalColor,
						"stroke" => "none",
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardBackgroundColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $breadboardWidth / 2),
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => ($globalColumns / 4 * $globalInterval) / -2,
												"y" => ($globalRows / 4 * $globalInterval) / -2,
												"width" => $globalColumns / 4 * $globalInterval,
												"height" => $globalRows / 4 * $globalInterval,
												"fill" => $breadboardShieldColor,
											),
										),
										array(
											"name" => "text",
											"attributes" => array(
												"x" => 0,
												"y" => fontBaselineOffset($breadboardTitleSize),
												"fill" => $breadboardTitleColor,
												"font-family" => $globalFontFamily,
												"font-size" => $breadboardTitleSize,
												"text-anchor" => "middle",
											),
											"tags" => $globalTitle,
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"cx" => ($globalColumns / 4 * $globalInterval) / -2 + 1.25,
												"cy" => ($globalRows / 4 * $globalInterval) / -2 + 1.25,
												"r" => 0.5,
												"fill" => $breadboardPokeColor,
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", ($globalRows - 1) * -1.25),
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"xlink:href" => "#row-chain-template",
														"transform" => sprintf("translate(%s,0)", $globalColumns * -1.25 - 2.5),
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"xlink:href" => "#row-chain-template",
														"transform" => sprintf("translate(%s,0)", $globalColumns * 1.25 + 2.5),
													),
												),
											),
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(%s,0)", ($globalColumns - 1) * -1.25),
											),
											"tags" => array(
												array(
													"name" => "use",
													"attributes" => array(
														"xlink:href" => "#column-chain-template",
														"transform" => sprintf("translate(0,%s)", $globalRows * -1.25 - 2.5),
													),
												),
												array(
													"name" => "use",
													"attributes" => array(
														"xlink:href" => "#column-chain-template",
														"transform" => sprintf("translate(0,%s)", $globalRows * 1.25 + 2.5),
													),
												),
											),
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => "translate(5,0)",
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2 - 5),
											),
											"tags" => $breadboardFrontTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2 + 5),
											),
											"tags" => $breadboardBackTerminalsChain,
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontPinsChain = array();
$pcbLeftTerminalsChain = array();
$pcbBottomPinsChain = array();
$pcbBottomTerminalsChain = array();
$pcbBackPinsChain = array();
$pcbRightTerminalsChain = array();
$pcbTopPinsChain = array();
$pcbTopTerminalsChain = array();
$pcbI = 0;
foreach ($globalPins as $key => $value) {
	if ($pcbI < $globalRows) {
		$pcbY = $pcbI * $globalInterval / 4;
		array_push($pcbFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#lr-pin-template",
				"transform" => sprintf("translate(0,%s)", $pcbY),
			),
		));
		array_push($pcbLeftTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $pcbY),
			),
		));
	} else if ($pcbI < $globalRows + $globalColumns) {
		$pcbX = ($pcbI - $globalRows) * $globalInterval / 4;
		array_push($pcbBottomPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#tb-pin-template",
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
		));
		array_push($pcbBottomTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
		));
	} else if ($pcbI < $globalRows * 2 + $globalColumns) {
		$pcbY = ($globalRows * 2 + $globalColumns - $pcbI - 1) * $globalInterval / 4;
		array_push($pcbBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#lr-pin-template",
				"transform" => sprintf("translate(0,%s)", $pcbY),
			),
		));
		array_push($pcbRightTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $pcbY),
			),
		));
	} else {
		$pcbX = (($globalRows + $globalColumns) * 2 - $pcbI - 1) * $globalInterval / 4;
		array_push($pcbTopPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#tb-pin-template",
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
		));
		array_push($pcbTopTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(%s,0)", $pcbX),
			),
		));
	}
	$pcbI++;
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 5,
						"y" => 5,
						"width" => $pcbWidth - 10,
						"height" => $pcbHeight - 10,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"cx" => 6.25,
						"cy" => 6.25,
						"r" => 0.5,
						"fill" => $pcbSilkscreenColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "text",
					"attributes" => array(
						"font-size" => $pcbTitleSize,
						"font-family" => $globalFontFamily,
						"text-anchor" => "middle",
						"x" => $pcbWidth / 2,
						"y" => $pcbHeight / 2 + fontBaselineOffset($pcbTitleSize),
						"fill" => $pcbSilkscreenColor,
						"stroke" => "none",
					),
					"tags" => $globalTitle,
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"opacity" => 0,
						"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"id" => "lr-pin-template",
								"x" => $pcbPinWidth / -2,
								"y" => $pcbPinHeight / -2,
								"width" => $pcbPinWidth,
								"height" => $pcbPinHeight,
								"fill" => "#FFBF00",
								"stroke" => "none",
							),
						),
						array(
							"name" => "rect",
							"attributes" => array(
								"id" => "tb-pin-template",
								"x" => $pcbPinHeight / -2,
								"y" => $pcbPinWidth / -2,
								"width" => $pcbPinHeight,
								"height" => $pcbPinWidth,
								"fill" => "#FFBF00",
								"stroke" => "none",
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,6.25)", $pcbWidth / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $pcbWidth / -2 + 2.5),
							),
							"tags" => $pcbFrontPinsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $pcbWidth / -2),
							),
							"tags" => $pcbLeftTerminalsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $pcbWidth / 2 - 2.5),
							),
							"tags" => $pcbBackPinsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,0)", $pcbWidth / 2),
							),
							"tags" => $pcbRightTerminalsChain,
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(6.25,%s)", $pcbHeight / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / 2 - 2.5),
							),
							"tags" => $pcbBottomPinsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / 2),
							),
							"tags" => $pcbBottomTerminalsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / -2 + 2.5),
							),
							"tags" => $pcbTopPinsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $pcbHeight / -2),
							),
							"tags" => $pcbTopTerminalsChain,
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper0",
				"class" => "bottom-layer",
			),
		),
	),
);

// schematic
$schematicLeftPinsChain = array();
$schematicLeftTerminalsChain = array();
$schematicLeftTextsChain = array();
$schematicBottomPinsChain = array();
$schematicBottomTerminalsChain = array();
$schematicBottomTextsChain = array();
$schematicRightPinsChain = array();
$schematicRightTerminalsChain = array();
$schematicRightTextsChain = array();
$schematicTopPinsChain = array();
$schematicTopTerminalsChain = array();
$schematicTopTextsChain = array();
$schematicI = 0;
foreach ($globalPins as $key => $value) {
	if ($schematicI < $globalRows) {
		$schematicY = $schematicI * $globalInterval;
		array_push($schematicLeftPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicLeftTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicLeftTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	} else if ($schematicI < $globalRows + $globalColumns) {
		$schematicY = ($schematicI - $globalRows) * $globalInterval;
		array_push($schematicBottomPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicBottomTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicBottomTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	} else if ($schematicI < $globalRows * 2 + $globalColumns) {
		$schematicY = ($globalRows * 2 + $globalColumns - $schematicI - 1) * $globalInterval;
		array_push($schematicRightPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicRightTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicRightTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	} else {
		$schematicY = (($globalRows + $globalColumns) * 2 - $schematicI - 1) * $globalInterval;
		array_push($schematicTopPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $key),
				"xlink:href" => "#pin-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicTopTerminalsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-terminal", $key),
				"xlink:href" => "#terminal-template",
				"transform" => sprintf("translate(0,%s)", $schematicY),
			),
		));
		array_push($schematicTopTextsChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => 0,
				"y" => $schematicY,
			),
			"tags" => $key,
		));
	}
	$schematicI++;
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / 4 / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / 4 / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
				"transform" => "translate(0.25,0.25)",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 20,
						"y" => 20,
						"width" => $schematicWidth - 40,
						"height" => $schematicHeight - 40,
						"fill" => $schematicBackgroundColor,
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"cx" => 25,
						"cy" => 30,
						"r" => 1,
						"fill" => $schematic1stPinColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(%s,%s)", $schematicWidth / 2, $schematicHeight / 2),
					),
					"tags" => array(
						array(
							"name" => "text",
							"attributes" => array(
								"fill" => $schematicTitleColor,
								"stroke" => "none",
								"font-family" => $globalFontFamily,
								"font-size" => $schematicTitleSize,
								"text-anchor" => "middle",
								"x" => 0,
								"y" => fontBaselineOffset($schematicTitleSize),
							),
							"tags" => $globalTitle,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $schematicHeight / -2 + 30),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $schematicWidth / -2 + 10),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,0)",
											),
											"tags" => $schematicLeftPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(-10,0)",
											),
											"tags" => $schematicLeftTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(-5,%s)", fontBaselineOffset($schematicTextSize) * -2),
												"fill" => $schematicTextColor,
												"font-family" => $globalFontFamily,
												"font-size" => $schematicTextSize,
												"stroke" => "none",
												"text-anchor" => "start",
											),
											"tags" => $schematicLeftTextsChain,
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $schematicWidth / 2 - 10),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,0)",
											),
											"tags" => $schematicRightPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(10,0)",
											),
											"tags" => $schematicRightTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(-5,%s)", fontBaselineOffset($schematicTextSize) * -2),
												"fill" => $schematicTextColor,
												"font-family" => $globalFontFamily,
												"font-size" => $schematicTextSize,
												"stroke" => "none",
												"text-anchor" => "start",
											),
											"tags" => $schematicRightTextsChain,
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("rotate(-90) translate(0,%s)", $schematicWidth / -2 + 30),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $schematicHeight / -2 + 10),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,0)",
											),
											"tags" => $schematicBottomPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(-10,0)",
											),
											"tags" => $schematicBottomTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(5,%s)", fontBaselineOffset($schematicTextSize) * -2),
												"fill" => $schematicTextColor,
												"font-family" => $globalFontFamily,
												"font-size" => $schematicTextSize,
												"stroke" => "none",
												"text-anchor" => "end",
											),
											"tags" => $schematicBottomTextsChain,
										),
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(%s,0)", $schematicHeight / 2 - 10),
									),
									"tags" => array(
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(0,0)",
											),
											"tags" => $schematicTopPinsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => "translate(10,0)",
											),
											"tags" => $schematicTopTerminalsChain,
										),
										array(
											"name" => "g",
											"attributes" => array(
												"transform" => sprintf("translate(-5,%s)", fontBaselineOffset($schematicTextSize) * -2),
												"fill" => $schematicTextColor,
												"font-family" => $globalFontFamily,
												"font-size" => $schematicTextSize,
												"stroke" => "none",
												"text-anchor" => "start",
											),
											"tags" => $schematicTopTextsChain,
										),
									),
								),
							),
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}