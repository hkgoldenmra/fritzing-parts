<?php

// global settings
$globalColor = "Red";
$globalPins = array(
	// 8x8
	"R" => array(8, 13, 7, 11, 0, 6, 1, 4),
	"C" => array(12, 2, 3, 9, 5, 10, 14, 15),
	// 5x7
//	"R" => array(0, 2, 9, 6, 7),
//	"C" => array(11, 10, 1, 8, 3, 4, 5),
);
$globalDirection = false;
$globalTitle = sprintf("LED Matrix DIP %s %sx%s %s", $globalColor, count($globalPins["R"]), count($globalPins["C"]), (($globalDirection) ? "Anode" : "Cathode"));
$globalFontFamily = "OCRA std";
$globalComment = "This Fritzing part is created by HKGoldenMr.A from PHP 7.2 with ZipArchive library.";

// XML settings
define("XML_VERSION", "1.0");
define("XML_ENCODING", "UTF-8");

// svg settings
define("SVG_VERSION", "1.1");
define("SVG_XML_NAMESPACE", "http://www.w3.org/2000/svg");
define("SVG_XML_NAMESPACE_XLINK", "http://www.w3.org/1999/xlink");

// fritzing settings
$fritzingVersion = "0.9.3b";
$fritzingAuthor = "HKGoldenMr.A";
$fritzingLabel = "MATRIX";
$fritzingTags = array("LED", "Matrix", "DIP");
$fritzingProperties = array(
	"color" => $globalColor,
	"columns" => count($globalPins["C"]),
	"common" => (($globalDirection) ? "Anode" : "Cathode"),
	"family" => "A LED Matrix",
	"package" => "DIP",
	"rows" => count($globalPins["R"]),
);

// icon settings
$iconUnit = "in";
$iconShieldColor = "#000000";
$iconLEDRadius = 5;
$iconLedThickness = 1;
$iconPinRadius = 2.5;

// breadboard settings
$breadboardUnit = $iconUnit;
$breadboardShieldColor = $iconShieldColor;
$breadboardLEDRadius = 7.5;
$breadboardLedThickness = $iconLedThickness;

// pcb settings
$pcbUnit = $iconUnit;
$pcbPinColor = "#FFBF00";
$pcbPinWidth = 1;
$pcbPinRadius = 3;
$pcbBackgroundColor = "none";
$pcbSilkscreenColor = "#000000";
$pcbSilkscreenThickness = 1;
$pcbTextSize = 5;

// schematic settings
$schematicUnit = $iconUnit;
$schematicBackgroundColor = "#FFFFFF";
$schematicBorderThickness = 1;
$schematicBorderColor = "#000000";
$schematicPinColor = "#888888";
$schematicPinThickness = 1;
$schematicTextSize = 8;
$schematicTextColor = $schematicPinColor;

// Don't change the code below, unless you know what to do...
function fontBaselineOffset($size) {
	return $size / 4;
}

function toXMLString(array $xml, int $tab = 0) {
	$string = "";
	if ($tab == 0) {
		$string .= sprintf('<?xml version="%s" encoding="%s"?>', XML_VERSION, XML_ENCODING);
		if ($xml["name"] == "svg") {
			$string .= "\n" . '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		}
	}
	$string .= sprintf("\n%s<%s", str_repeat("\t", $tab), $xml["name"]);
	if (array_key_exists("attributes", $xml)) {
		ksort($xml["attributes"]);
		foreach ($xml["attributes"] as $key => $value) {
			$string .= sprintf(' %s="%s"', $key, $value);
		}
	}
	if (array_key_exists("tags", $xml)) {
		if (is_array($xml["tags"])) {
			if (count($xml["tags"]) > 0) {
				$string .= ">";
				foreach ($xml["tags"] as $tag) {
					$string .= toXMLString($tag, $tab + 1);
				}
				$string .= sprintf("\n%s</%s>", str_repeat("\t", $tab), $xml["name"]);
			} else {
				$string .= "/>";
			}
		} else {
			if (strlen($xml["tags"]) > 0) {
				$string .= sprintf(">%s</%s>", $xml["tags"], $xml["name"]);
			} else {
				$string .= "/>";
			}
		}
	} else {
		$string .= "/>";
	}
	return $string;
}

function uuidgen($prefix = "") {
	$string = md5(uniqid(mt_rand(), true));
	$uuid = substr($string, 0, 8) . '-';
	$uuid .= substr($string, 8, 4) . '-';
	$uuid .= substr($string, 12, 4) . '-';
	$uuid .= substr($string, 16, 4) . '-';
	$uuid .= substr($string, 20, 12);
	return $prefix . $uuid;
}

$globalInterval = 10;
$breadboardInterval = 1.5;
$iconWidth = 40;
$iconHeight = 40;
$breadboardWidth = count($globalPins["C"]) * $globalInterval * $breadboardInterval;
$breadboardHeight = count($globalPins["R"]) * $globalInterval * $breadboardInterval;
$pcbWidth = $breadboardWidth;
$pcbHeight = $breadboardHeight;
$schematicWidth = count($globalPins["C"]) * 30 + 5;
$schematicHeight = count($globalPins["R"]) * 30 + 5;

$now = getdate();
$multiple = 100;

$filenameBasename = sprintf("led-matrix-dip-%s-%sx%s-%s", $globalColor, count($globalPins["R"]), count($globalPins["C"]), (($globalDirection) ? "Anode" : "Cathode"));
$filenameFZPZ = sprintf("%s.fzpz", $filenameBasename);
$filenamePart = sprintf("part.%s.fzp", $filenameBasename);
$filenameIcon = sprintf("svg.icon.%s.svg", $filenameBasename);
$filenameBreadboard = sprintf("svg.breadboard.%s.svg", $filenameBasename);
$filenamePCB = sprintf("svg.pcb.%s.svg", $filenameBasename);
$filenameSchematic = sprintf("svg.schematic.%s.svg", $filenameBasename);

// part
$fritzingTagsChain = array();
$fritzingPropertiesChain = array();
$fritzingConnectorsChain = array();
foreach ($fritzingTags as $value) {
	array_push($fritzingTagsChain, array(
		"name" => "tag",
		"tags" => $value,
	));
}
foreach ($fritzingProperties as $key => $value) {
	array_push($fritzingPropertiesChain, array(
		"name" => "property",
		"attributes" => array(
			"name" => $key,
		),
		"tags" => $value
	));
}
$fritzingPins = array();
foreach ($globalPins as $key => $array) {
	foreach ($array as $index => $value) {
		$fritzingPins[$value] = array("name" => $key . ($index + 1), "direction" => ($key == "R" && $globalDirection) || ($key == "C" && !$globalDirection));
	}
}
ksort($fritzingPins);
foreach ($fritzingPins as $value) {
	$id = sprintf("connector-%s", $value["name"]);
	array_push($fritzingConnectorsChain, array(
		"name" => "connector",
		"attributes" => array(
			"id" => $id,
			"name" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			"type" => "male",
		),
		"tags" => array(
			array(
				"name" => "description",
				"tags" => sprintf("%s %s", $value["name"], (($value["direction"]) ? "Anode" : "Cathode")),
			),
			array(
				"name" => "views",
				"tags" => array(
					array(
						"name" => "breadboardView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "breadboard",
								),
							),
						),
					),
					array(
						"name" => "schematicView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "schematic",
								),
							),
						),
					),
					array(
						"name" => "pcbView",
						"tags" => array(
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper1",
								),
							),
							array(
								"name" => "p",
								"attributes" => array(
									"svgId" => sprintf("%s-pin", $id),
//									"terminalId" => sprintf("%s-terminal", $id),
//									"legId" => sprintf("%s-leg", $id),
									"layer" => "copper0",
								),
							),
						),
					),
				),
			),
		),
	));
}
$part = array(
	"name" => "module",
	"attributes" => array(
		"fritzingVersion" => $fritzingVersion,
		"moduleId" => uuidgen(),
	),
	"tags" => array(
		array(
			"name" => "version",
			"tags" => 4,
		),
		array(
			"name" => "author",
			"tags" => $fritzingAuthor,
		),
		array(
			"name" => "title",
			"tags" => $globalTitle,
		),
//		array(
//			"name" => "description",
//			"tags" => $globalDescription,
//		),
		array(
			"name" => "date",
			"tags" => sprintf("%04d-%02d-%02d", $now["year"], $now["mon"], $now["mday"]),
		),
		array(
			"name" => "label",
			"tags" => $fritzingLabel,
		),
		array(
			"name" => "tags",
			"tags" => $fritzingTagsChain,
		),
		array(
			"name" => "properties",
			"tags" => $fritzingPropertiesChain,
		),
		array(
			"name" => "views",
			"tags" => array(
				array(
					"name" => "iconView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("icon/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "icon",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "breadboardView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("breadboard/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "breadboard",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "schematicView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("schematic/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "schematic",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "pcbView",
					"tags" => array(
						array(
							"name" => "layers",
							"attributes" => array(
								"image" => sprintf("pcb/%s.svg", $filenameBasename),
							),
							"tags" => array(
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper1",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "silkscreen",
									),
								),
								array(
									"name" => "layer",
									"attributes" => array(
										"layerId" => "copper0",
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			"name" => "connectors",
			"tags" => $fritzingConnectorsChain,
		),
	),
);

// icon
$iconColumnLEDsChain = array();
$iconRowLEDsChain = array();
for ($iconI = 0; $iconI < $iconWidth; $iconI += $globalInterval) {
	array_push($iconColumnLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0)", $iconI),
		),
	));
}
for ($iconI = 0; $iconI < $iconHeight; $iconI += $globalInterval) {
	array_push($iconRowLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#row-led-template",
			"transform" => sprintf("translate(0,%s)", $iconI),
		),
	));
}
$icon = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $iconWidth / $multiple, $iconUnit),
		"height" => sprintf("%s%s", $iconHeight / $multiple, $iconUnit),
		"viewBox" => sprintf("0 0 %s %s", $iconWidth, $iconHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "led-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $iconLEDRadius,
						"fill" => $globalColor,
						"stroke" => $iconShieldColor,
						"stroke-width" => $iconLedThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "row-led-template",
					),
					"tags" => $iconColumnLEDsChain,
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "icon",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $iconWidth,
								"height" => $iconHeight,
								"fill" => $iconShieldColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf('translate(%1$s,%1$s)', $iconLEDRadius),
							),
							"tags" => $iconRowLEDsChain,
						),
					),
				),
			),
		),
	),
);

// breadboard
$breadboardColumnLEDsChain = array();
$breadboardRowLEDsChain = array();
$breadboardFrontPinsChain = array();
$breadboardBackPinsChain = array();
for ($breadboardI = 0; $breadboardI < $breadboardWidth; $breadboardI += $breadboardLEDRadius * 2) {
	array_push($breadboardColumnLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0)", $breadboardI),
		),
	));
}
for ($breadboardI = 0; $breadboardI < $breadboardHeight; $breadboardI += $breadboardLEDRadius * 2) {
	array_push($breadboardRowLEDsChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#row-led-template",
			"transform" => sprintf("translate(0,%s)", $breadboardI),
		),
	));
}
$breadboardI = 0;
foreach ($fritzingPins as $value) {
	if ($breadboardI < count($fritzingPins) / 2) {
		array_push($breadboardFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", $breadboardI * $globalInterval),
				"xlink:href" => (($breadboardI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		array_push($breadboardBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", (count($fritzingPins) - $breadboardI - 1) * $globalInterval),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$breadboardI++;
}
$breadboard = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $breadboardWidth / $multiple, $breadboardUnit),
		"height" => sprintf("%s%s", $breadboardHeight / $multiple, $breadboardUnit),
		"viewBox" => sprintf("0 0 %s %s", $breadboardWidth, $breadboardHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "led-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $breadboardLEDRadius,
						"fill" => $globalColor,
						"stroke" => $breadboardShieldColor,
						"stroke-width" => $iconLedThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "row-led-template",
					),
					"tags" => $breadboardColumnLEDsChain,
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "pin-template",
						"cx" => 0,
						"cy" => 0,
						"r" => $iconPinRadius,
						"opacity" => 0,
					),
				),
				array(
					"name" => "rect",
					"attributes" => array(
						"id" => "1st-pin-template",
						"x" => -$iconPinRadius,
						"y" => -$iconPinRadius,
						"width" => $iconPinRadius * 2,
						"height" => $iconPinRadius * 2,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "breadboard",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "rect",
							"attributes" => array(
								"x" => 0,
								"y" => 0,
								"width" => $breadboardWidth,
								"height" => $breadboardHeight,
								"fill" => $breadboardShieldColor,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf('translate(%1$s,%1$s)', $breadboardLEDRadius)
							),
							"tags" => $breadboardRowLEDsChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $breadboardWidth / 2 - ((count($globalPins["R"]) + count($globalPins["C"])) / 2 - 1) * 5, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2 - $globalInterval * 3 / 2),
									),
									"tags" => $breadboardFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2 + $globalInterval * 3 / 2),
									),
									"tags" => $breadboardBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// pcb
$pcbFrontTextChain = array();
$pcbBackTextChain = array();
$pcbFrontPinsChain = array();
$pcbBackPinsChain = array();
$pcbI = 0;
foreach ($fritzingPins as $value) {
	if ($pcbI < count($fritzingPins) / 2) {
		array_push($pcbFrontTextChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => $pcbI * $globalInterval,
				"y" => 0,
			),
			"tags" => $value["name"],
		));
		array_push($pcbFrontPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", $pcbI * $globalInterval),
				"xlink:href" => (($pcbI > 0) ? "#pin-template" : "#1st-pin-template"),
			),
		));
	} else {
		array_push($pcbBackTextChain, array(
			"name" => "text",
			"attributes" => array(
				"x" => (count($fritzingPins) - $pcbI - 1) * $globalInterval,
				"y" => 0,
			),
			"tags" => $value["name"],
		));
		array_push($pcbBackPinsChain, array(
			"name" => "use",
			"attributes" => array(
				"id" => sprintf("connector-%s-pin", $value["name"]),
				"transform" => sprintf("translate(%s,0)", (count($fritzingPins) - $pcbI - 1) * $globalInterval),
				"xlink:href" => "#pin-template",
			),
		));
	}
	$pcbI++;
}
$pcb = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $pcbWidth / $multiple, $pcbUnit),
		"height" => sprintf("%s%s", $pcbHeight / $multiple, $pcbUnit),
		"viewBox" => sprintf("0 0 %s %s", $pcbWidth, $pcbHeight),
	),
	"tags" => array(
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "silkscreen",
			),
			"tags" => array(
				array(
					"name" => "rect",
					"attributes" => array(
						"x" => 0,
						"y" => 0,
						"width" => $pcbWidth,
						"height" => $pcbHeight,
						"fill" => $pcbBackgroundColor,
						"stroke" => $pcbSilkscreenColor,
						"stroke-width" => $pcbSilkscreenThickness,
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $pcbTextSize,
						"font-family" => $globalFontFamily,
						"text-anchor" => "middle",
						"transform" => sprintf("translate(%s,%s)", $breadboardWidth / 2 - ((count($globalPins["R"]) + count($globalPins["C"])) / 2 - 1) * 5, $breadboardHeight / 2),
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2 - $globalInterval * 2),
							),
							"tags" => $pcbFrontTextChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2 + $globalInterval * 2 + $pcbTextSize - fontBaselineOffset($pcbTextSize)),
							),
							"tags" => $pcbBackTextChain,
						),
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "copper1",
				"class" => "top-layer",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "copper0",
						"class" => "bottom-layer",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"opacity" => 0,
								"transform" => sprintf("translate(%s,%s)", $pcbWidth * 100, $pcbHeight * 100),
							),
							"tags" => array(
								array(
									"name" => "circle",
									"attributes" => array(
										"id" => "pin-template",
										"cx" => 0,
										"cy" => 0,
										"r" => $pcbPinRadius,
										"fill" => $pcbBackgroundColor,
										"stroke" => $pcbPinColor,
										"stroke-width" => $pcbPinWidth,
									),
								),
								array(
									"name" => "g",
									"attributes" => array(
										"id" => "1st-pin-template",
									),
									"tags" => array(
										array(
											"name" => "rect",
											"attributes" => array(
												"x" => -$pcbPinRadius,
												"y" => -$pcbPinRadius,
												"width" => $pcbPinRadius * 2,
												"height" => $pcbPinRadius * 2,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
										array(
											"name" => "circle",
											"attributes" => array(
												"id" => "pin-template",
												"cx" => 0,
												"cy" => 0,
												"r" => $pcbPinRadius,
												"fill" => $pcbBackgroundColor,
												"stroke" => $pcbPinColor,
												"stroke-width" => $pcbPinWidth,
											),
										),
									),
								),
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => sprintf("translate(%s,%s)", $breadboardWidth / 2 - ((count($globalPins["R"]) + count($globalPins["C"])) / 2 - 1) * 5, $breadboardHeight / 2),
							),
							"tags" => array(
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $breadboardHeight / 2 - $globalInterval * 3 / 2),
									),
									"tags" => $pcbFrontPinsChain,
								),
								array(
									"name" => "g",
									"attributes" => array(
										"transform" => sprintf("translate(0,%s)", $breadboardHeight / -2 + $globalInterval * 3 / 2),
									),
									"tags" => $pcbBackPinsChain,
								),
							),
						),
					),
				),
			),
		),
	),
);

// schematic
$schematicColumnLinesChain = array();
$schematicRowLinesChain = array();
$schematicJointsColumnChain = array();
$schematicJointsRowChain = array();
$schematicLEDsColumnChain = array();
$schematicLEDsRowChain = array();
$schematicPinsColumnChain = array();
$schematicPinsRowChain = array();
$schematicTerminalsColumnChain = array();
$schematicTerminalsRowChain = array();
$schematicTextsColumnChain = array();
$schematicTextsRowChain = array();
$schematicI = 0;
foreach ($globalPins["C"] as $key => $value) {
	array_push($schematicColumnLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#column-line-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicJointsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-template",
			"transform" => sprintf("translate(%s,0) rotate(-45)%s", $schematicI * 30, (($globalDirection) ? "" : " scale(1,-1)")),
		),
	));
	array_push($schematicPinsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-C%s-pin", $key + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(%s,0) rotate(90)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsColumnChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-C%s-terminal", $key + 1),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
	));
	array_push($schematicTextsColumnChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(%s,0)", $schematicI * 30),
		),
		"tags" => $value + 1,
	));
	$schematicI++;
}
$schematicI = 0;
foreach ($globalPins["R"] as $key => $value) {
	array_push($schematicRowLinesChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#row-line-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicJointsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#joint-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicLEDsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"xlink:href" => "#led-row-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicPinsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-R%s-pin", $key + 1),
			"xlink:href" => "#pin-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTerminalsRowChain, array(
		"name" => "use",
		"attributes" => array(
			"id" => sprintf("connector-R%s-terminal", $key + 1),
			"xlink:href" => "#terminal-template",
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
	));
	array_push($schematicTextsRowChain, array(
		"name" => "text",
		"attributes" => array(
			"transform" => sprintf("translate(0,%s)", $schematicI * 30),
		),
		"tags" => $value + 1,
	));
	$schematicI++;
}
$schematic = array(
	"name" => "svg",
	"attributes" => array(
		"version" => SVG_VERSION,
		"xmlns" => SVG_XML_NAMESPACE,
		"xmlns:xlink" => SVG_XML_NAMESPACE_XLINK,
		"width" => sprintf("%s%s", $schematicWidth / $multiple, $schematicUnit),
		"height" => sprintf("%s%s", $schematicHeight / $multiple, $schematicUnit),
		"viewBox" => sprintf("0 0 %s %s", $schematicWidth, $schematicHeight),
	),
	"tags" => array(
		array(
			"name" => "defs",
			"tags" => array(
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "row-line-template",
						"points" => sprintf("0,0 %s,0", (count($globalPins["C"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "column-line-template",
						"points" => sprintf("0,0 0,%s", (count($globalPins["R"]) - 1) * 30),
						"fill" => "none",
						"stroke" => $schematicBorderColor,
						"stroke-width" => $schematicBorderThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "joint-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 2,
						"fill" => $schematicBorderColor,
						"stroke" => "none",
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "joint-row-template",
					),
					"tags" => $schematicJointsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "direction-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-1,0 1,0 0,2",
								"fill" => $schematicBorderColor,
								"stroke" => "none",
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-2 0,0",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness / 2,
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-template",
					),
					"tags" => array(
						array(
							"name" => "polygon",
							"attributes" => array(
								"points" => "-4,-5 4,-5 0,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "0,-10 0,10",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "polyline",
							"attributes" => array(
								"points" => "-4,5 4,5",
								"fill" => "none",
								"stroke" => $schematicBorderColor,
								"stroke-width" => $schematicPinThickness,
							),
						),
						array(
							"name" => "g",
							"attributes" => array(
								"transform" => "translate(-3,8)",
							),
							"tags" => array(
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,0) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
								array(
									"name" => "use",
									"attributes" => array(
										"transform" => "translate(0,2) rotate(45)",
										"xlink:href" => "#direction-template",
									),
								),
							),
						),
					),
				),
				array(
					"name" => "g",
					"attributes" => array(
						"id" => "led-row-template",
					),
					"tags" => $schematicLEDsColumnChain,
				),
				array(
					"name" => "polyline",
					"attributes" => array(
						"id" => "pin-template",
						"points" => "-10,0 10,0",
						"fill" => "none",
						"stroke" => $schematicPinColor,
						"stroke-width" => $schematicPinThickness,
					),
				),
				array(
					"name" => "circle",
					"attributes" => array(
						"id" => "terminal-template",
						"cx" => 0,
						"cy" => 0,
						"r" => 0.001,
						"opacity" => 0,
					),
				),
			),
		),
		array(
			"name" => "g",
			"attributes" => array(
				"id" => "schematic",
			),
			"tags" => array(
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicColumnLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicRowLinesChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(27.5,7.5)",
					),
					"tags" => $schematicLEDsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight - 10),
					),
					"tags" => $schematicPinsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(10,0)",
					),
					"tags" => $schematicPinsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => sprintf("translate(35,%s)", $schematicHeight),
					),
					"tags" => $schematicTerminalsColumnChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(0,0)",
					),
					"tags" => $schematicTerminalsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(20,0)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"transform" => "translate(35,15)",
					),
					"tags" => $schematicJointsRowChain,
				),
				array(
					"name" => "g",
					"attributes" => array(
						"font-size" => $schematicTextSize,
						"font-family" => $globalFontFamily,
						"fill" => $schematicTextColor,
						"stroke" => "none",
					),
					"tags" => array(
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "end",
								"transform" => sprintf("translate(33,%s)", $schematicHeight - 2),
							),
							"tags" => $schematicTextsColumnChain,
						),
						array(
							"name" => "g",
							"attributes" => array(
								"text-anchor" => "start",
								"transform" => "translate(2,10)",
							),
							"tags" => $schematicTextsRowChain,
						),
					),
				),
			),
		),
	),
);

// preview
$preview = $breadboard;
$preview["attributes"]["width"] = (str_replace($breadboardUnit, "", $preview["attributes"]["width"]) * 10) . $breadboardUnit;
$preview["attributes"]["height"] = (str_replace($breadboardUnit, "", $preview["attributes"]["height"]) * 10) . $breadboardUnit;

$export = array(
	"parts" => $part,
	"icon" => $icon,
	"breadboard" => $breadboard,
	"pcb" => $pcb,
	"schematic" => $schematic,
	"preview" => $preview,
	"dist" => null,
);
foreach ($export as $key => $value) {
	if (!file_exists($key)) {
		mkdir($key, 0755);
	}
	if ($key == "dist" || $value === null) {
		$zip = new ZipArchive();
		$zip->open(sprintf("%s/%s", $key, $filenameFZPZ), ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->setArchiveComment($globalComment);
		$zip->addFromString($filenamePart, toXMLString($part));
		$zip->addFromString($filenameIcon, toXMLString($icon));
		$zip->addFromString($filenameBreadboard, toXMLString($breadboard));
		$zip->addFromString($filenamePCB, toXMLString($pcb));
		$zip->addFromString($filenameSchematic, toXMLString($schematic));
		$zip->close();
	} else if ($key == "parts") {
		file_put_contents(sprintf("%s/%s.fzp", $key, $filenameBasename), toXMLString($value));
	} else {
		file_put_contents(sprintf("%s/%s.svg", $key, $filenameBasename), toXMLString($value));
	}
}